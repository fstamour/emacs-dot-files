
.PHONY: debug-init
debug-init:
	emacs --debug-init

.PHONY: install
install:
	$(CURDIR)/check-install.sh

.PHONY: clean
clean:
	rm -rf $(CURDIR)/elpa
	rm -rf $(CURDIR)/eln-cache


.PHONY: clean-autosave-and-backups
clean-autosave:
	rm -rf $(CURDIR)/auto-save/
	rm -rf $(CURDIR)/auto-save-list/
	rm -rf $(CURDIR)/backups/
