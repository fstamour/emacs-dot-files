# Container to tests the configurations
#
# N.B. Use test.sh to build and run the container
#
# use `docker run -it --rm emacs.d` to run the container interactively


FROM alpine:3.11

# Install emacs
RUN apk add --no-cache emacs
RUN apk add --no-cache git
RUN apk add --no-cache w3m

# Create a non-root user
RUN adduser emacs --disabled-password
WORKDIR /home/emacs/
USER emacs

# Copy configurations
RUN mkdir /home/emacs/.emacs.d
COPY --chown=emacs:emacs . .emacs.d

ENTRYPOINT [ "emacs", "--debug-init" ]
