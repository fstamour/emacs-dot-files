#!/bin/sh

# stop on first error
set -e

# false => includes every files
# true  => doesn't include files ignored by .gitignore
clean=true
# clean=false

if [ $clean = "true" ]; then
	echo 'Creating a clean image (no installed package, no compiled files)'
	cat dockerignore .gitignore > .dockerignore
else
	echo 'Creating an image from current state (with installed packages and compiled files)'
	cat dockerignore > .dockerignore
fi

dockerImage=$(docker build . -t emacs.d | tee docker-build-logs | awk '/^Successfully built .*$/{ print $3 }')
echo dockerImage=$dockerImage
docker run -it --rm $dockerImage --batch --eval '(condition-case e (progn (load "~/.emacs.d/init.el") (message "-OK-")) (error (message "ERROR!") (signal (car e) (cdr e))))'
