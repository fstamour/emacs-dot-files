#!/usr/bin/env bash

function probe () {
  if [ -e "$1" ] || [ -d "$1" ]; then
    if [ -L "$1" ]; then
      echo "'$1' exists and is a symlink to '$(readlink -f "$1")'"
    else
      echo "'$1' exists."
    fi
    test -z "$2" || echo $2
  else
    echo "'$1' doesn't exists."
    test -z "$3" || echo $3
  fi
}

echo "Looking for suspicious files in ~"
for file in $(ls -a ~ | grep macs); do
  probe ~/$file
done

echo
echo "Looking for specific files"
probe ~/.emacs "You should move it into '~/.emacs.d'"
probe ~/.emacs.d "" "Maybe run 'ln -s $(readlink -f $(dirname $0)) ~/.emacs.d/'"

