# fstamour's Emacs configurations

## Setup

```
git clone git@gitlab.com:fstamour/emacs-dot-files.git ~/.emacs.d/
cd ~/.emacs.d/
make install
```

## How to update installed packages

```
M-x list-packages U x
```

## See notes.org
