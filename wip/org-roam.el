;; -*- lexical-binding: t; -*-

org-roam-db--table-schemata

;; List all tables
(mapcar 'car org-roam-db--table-schemata)
;; => (files nodes aliases citations refs tags links)

;; Get the "nodes" table's columns
(alist-get 'files org-roam-db--table-schemata)
;; => ([(file :unique :primary-key) title (hash :not-null) (atime :not-null) (mtime :not-null)])




;; TODO try out https://github.com/hasu/notdeft


;; TODO Find notes with duplicate title

(defun fsta/roam-duplicate-titles ()
  (org-roam-db-query [:select
                      [title (as (funcall count *) c)]
                      :from nodes
                      :group-by title
                      :having (< 1 c)]))


(defun fsta/roam/visit-id (id)
  (org-roam-node-visit (org-roam-populate
                        (org-roam-node-create :id id))))

;; TODO Create a (minor?) mode; add keymap for mouse and ret; don't
;; use the 'keymap property.
(let ((buffer-name "*org-roam-duplicates*"))
  (with-current-buffer (get-buffer-create buffer-name)
    (erase-buffer)
    (cl-loop for (title count) in (fsta/roam-duplicate-titles)
             do
             (insert title "\n")
             (cl-loop for (id file) in
                      ;; Find nodes by title (exact match)
                      (org-roam-db-query [:select
                                          [id file]
                                          :from nodes
                                          :where (= title $s1)]
                                         title)
                      do
                      (let ((map (make-sparse-keymap))
                            (action (lambda (id)
                                      (lambda ()
                                        (interactive)
                                        (message "Visiting node %s..." id)
                                        (fsta/roam/visit-id id)))))
                        (define-key map [mouse-1] (funcall action id))
                        (define-key map [?\r] (funcall action id))
                        (insert
                         (propertize
                          (concat  " - " (file-name-base file) " "
                                   "   " id)
                          'keymap map
                          'mouse-face 'highlight
                          'help-echo (format "mouse-2: visit the node %s" id))
                         "\n")))))
  (display-buffer buffer-name))



;; Find notes by title prefix
(defun fsta/roam/find-note-like (pattern)
  (org-roam-db-query [:select
                      [title]
                      :from nodes
                      :where (like title $s1)]
                     pattern))

;; TODO
(fsta/roam/find-note-like "Related%")

(org-roam-node-find )

;; org-roam-node-visit expects a node _structure_; see
;; org-roam-node-list on how to build one from the database.


;;; Find org files that contains no notes (this indicates some issue)

(let ((buffer-name "*org-roam-ignored-files*"))
  (with-current-buffer (get-buffer-create buffer-name)
    (erase-buffer)
    (let* ((in-db (mapcar 'car (org-roam-db-query
                                ;; [:select file :from files]
                                [:select file :from nodes :group-by file])))
           (all (directory-files org-roam-directory t "^[^.].*\\.org$"))
           (ignored (remove-if (lambda (file) (member file in-db)) all)))
      (insert (format "There are %s files in org-roam's db.\n" (length in-db))
              (format "There are %s files in %S.\n" (length all) org-roam-directory)
              (format "There's a difference of %s files.\n" (- (length all) (length in-db)))
              "\n")
      (loop for file in ignored
            do (let ((map (make-sparse-keymap))
                     (action (lambda (file)
                               (lambda ()
                                 (interactive)
                                 (message "Visiting file %s..." file)
                                 (find-file file)))))
                 (define-key map [mouse-1] (funcall action file))
                 (define-key map [?\r] (funcall action file))
                 (insert
                  (propertize file
                              'keymap map
                              'mouse-face 'highlight
                              'help-echo (format "mouse-2: visit the file %s" file))
                  "\n")))))
  (display-buffer buffer-name))



;;; List all tags

(org-roam-db-query
 ;; [:select file :from files]
 [:select tag :from tags :group-by tag])



;;; Temporarily bind <f2>

(defun fsta/buffer-match (regexp)
  (save-excursion
    (save-match-data
      (goto-char (point-min))
      (re-search-forward regexp nil t))))

(defun fsta/roam/ignored-file ()
  (let*  ((in-db (mapcar 'car (org-roam-db-query
                               ;; [:select file :from files]
                               [:select file :from nodes :group-by file])))
          (all (directory-files org-roam-directory t "^[^.].*\\.org$"))
          (ignored (remove-if (lambda (file)
                                (or (member file in-db)
                                    (string= "captured"
                                             (file-name-base file))))
                              all)))
    ignored))

;; TODO see wip/notes.el's note/infer-title-from-file-name
(defun fsta/roam/maybe-add-title ()
  (save-excursion
    (beginning-of-buffer)
    (unless (let ((case-fold-search t))
              (fsta/buffer-match "^#\\+title:"))
      (insert "#+title: " (file-name-base buffer-file-name) "\n"))))

(defun fsta/roam/maybe-fixup-tags ()
  (save-excursion
    (save-match-data
      (beginning-of-buffer)
      (when (let ((case-fold-search t))
              (re-search-forward "^#\\+tags: \\( *#\\(.*\\)\\)+$" nil t))
        ;; (message "bad tags: %S" (match-string-no-properties 1))
        (let ((replacement (save-match-data
                             (concat ":"
                                     (string-join
                                      (split-string (match-string-no-properties 1) " ?#" t)
                                      ":")
                                     ":"))))
          (replace-match
           replacement
           t t nil 1))))))

(defun fsta/roam/dwim ()
  (interactive)
  (let ((next (cl-first (fsta/roam/ignored-file))))
    (cond
     ((and buffer-file-name
           (string= buffer-file-name next))
      ;; Each step must be idempotent to not be a PITA to use
      (fsta/roam/maybe-add-title)
      (fsta/roam/maybe-fixup-tags)
      ;; progn is just to show that these go togheter
      (progn (beginning-of-buffer) (org-id-get-create))
      (save-buffer)
      )
     (t
      (find-file-other-window next)))))

;; (define-key org-mode-map (kbd "<f2>") 'fsta/roam/dwim)

(define-key fstamour-minor-mode-map (kbd "<f2>") 'fsta/roam/dwim)



;;; Capture templates

;; Here's the default list of capture templates
org-roam-capture-templates
(("d" "default" plain "%?" :target (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}
") :unnarrowed t))

;; I don't want any timestamps
(setq org-roam-capture-templates
      '(("d" "default" plain "%?" :target (file+head "${slug}.org" "#+title: ${title}
") :unnarrowed t)))



;;; md-roam - org-roam plugin to use org-roam with markdown


;; I'm vagely mad that I can't install straight with this xS
(use-package straight)
;; https://github.com/radian-software/straight.el

(use-package md-roam)
;; https://github.com/nobiot/md-roam


;; TODO (ironically) make a note for: https://notes.alexkehayias.com/querying-stats-from-org-roam/
