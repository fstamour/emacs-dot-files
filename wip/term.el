
(make-comint-in-buffer
 ;; name
 "bash"
 ;; buffer
 nil
 ;; program
 "bash"
 ;; startfile
 nil
 ;; &rest switches
 "-li"
 )


term-exec-1

term


(setf explicit-shell-file-name
      (expand-file-name
       (concat
	(file-name-directory
	 (executable-find "git"))
	"../usr/bin/bash.exe")))

(term explicit-shell-file-name)
