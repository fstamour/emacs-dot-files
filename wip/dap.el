;;; it looks like dap-mode requires lsp-mode, and doesn't work with eglot at all
(require 'dap-mode)

(add-hook 'dap-stopped-hook
          (lambda (arg) (call-interactively #'dap-hydra)))

(defvar fsta/dap-debug-template-configurations-backup dap-debug-template-configurations)

(setq dap-debug-template-configurations nil)

(dap-register-debug-template
 "Launch"
 (list :type "go"
       :request "launch"
       :name "Launch"
       :mode "exec"
       :program nil
       :args nil
       :env nil
       ))

;; (require 'dap-go) ;; deprecated


;;; It might still be useful to look at this package for example of
;;; configurations
(require 'dap-dlv-go)


;;;


;; we must explicitly load jsonrpc to get a newer version that the one
;; built-in in emacs. the newer version must be loaded before dape
;;
;; Update, this workaround didn't work
(require 'jsonrpc)

;; Force-load jsonrpc from the *.el file
(load (find-library-name "jsonrpc"))

;; (find-library-name "dape")
;; 🤦
(load "/home/fstamour/.emacs.d/straight/build/el-patch/dape.el")

;; The default config for delve:
(alist-get 'dlv dape-configs)
(modes (go-mode go-ts-mode)
       ensure dape-ensure-command
       command "dlv"
       command-args ("dap" "--listen" "127.0.0.1::autoport")
       command-cwd dape-command-cwd
       command-insert-stderr t
       port :autoport
       :request "launch"
       :type "debug"
       :cwd "."
       :program ".")


(defun fsta/current-go-package ()
  (concat "./" (file-relative-name
                default-directory
                (funcall dape-cwd-fn))))

(cl-pushnew
 `(go-debug-test
   modes (go-mode go-ts-mode)
   command ,(expand-file-name "~/go/bin/dlv")
   command-args ("dap" "--listen" "127.0.0.1::autoport")
   command-cwd dape-command-cwd
   port :autoport
   :name "Debug Go tests"
   :request "launch"
   :type "go"
   :mode "test"
   :cwd dape-cwd-fn
   :program fsta/current-go-package
   :args [])
 dape-configs
 :key #'car)

(car dape-configs)
(pop dape-configs)
