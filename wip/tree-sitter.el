
(treesit-available-p)
;; => t

(treesit-language-available-p 'typescript)
;; => t

(treesit-language-available-p 'docker)


;;; https://www.masteringemacs.org/article/how-to-get-started-tree-sitter

treesit-extra-load-path
;; => nil

;; Emacs will also look there:
(expand-file-name "tree-sitter" user-emacs-directory)
;; => "/home/fstamour/.emacs.d/tree-sitter"

;; Build and install the tree-sitter language grammar library for LANG.
;; M-x treesit-install-language-grammar


;; (mapc #'treesit-install-language-grammar (mapcar #'car treesit-language-source-alist))
