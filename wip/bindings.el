;;;; Help find available key-bindings
;;;; Conversly, can help _discover_ key bindings.

(cl-loop for c from ?a upto ?z
	 for keys = (format "C-M-%s" (char-to-string c))
	 for key-binding = (key-binding (kbd keys))
	 unless key-binding
	 collect (list keys key-binding))
