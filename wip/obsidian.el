obsidian-directory


(obsidian-cache-needs-reset-p) returns t after a certain time
obsidian-cache-expiry ;; => 3600


(fsta/measure-time
 (obsidian-clear-cache)
 (obsidian-reset-cache)
 (length obsidian-files-cache)) ; is slow because this is slow too:
;; Was: 3s
;; Now: 0.06s

(length (directory-files-recursively obsidian-directory "\.*$"))
;; => 40702

(length obsidian-files-cache)
;; Was: 2020
;; Now: 120

(inspector-inspect obsidian-files-cache)

(obsidian-file-p obsidian-directory)
(obsidian-file-p (file-name-concat obsidian-directory "node_modules"))

(let ((path (file-name-concat obsidian-directory "node_modules")))
  (s-contains-p "node_modules" path))

(length
 (directory-files-recursively
  obsidian-directory "\.*$"
  nil
  (lambda (fullpath)
    ;; .direnv
    ;; .git
    ;; see .gitignore
    ;; node_modules
    (let ((case-fold-search t)
          ;; (relative-path (file-relative-name fullpath obsidian-directory))
          )
      (or (not (obsidian-user-directory-p fullpath))
          (not (string-match-p (rx (or "node_modules")) fullpath)))
      ))))
;; => 587

(fsta/measure-time
 (obsidian-clear-cache)
 (obsidian--update-all-from-front-matter))
;; Originally: 3.5s
;; Filtering out node_modules and .git: 0.6s
;; Not using ->> and -filter (e.g. passing #'obsidian-file-p to directory-files-recursively: 0.1s
;; using magit: 0.07s

(obsidian-list-all-directories) ; is also slow

(use-package inspector)

(inspector-inspect obsidian--aliases-map)

(hash-table-count obsidian--aliases-map)


(magit-ignored-files)
(magit-file-ignored-p )




obsidian-jump call obsidian-update, which calls
- (obsidian-reset-cache)
- (obsidian-update-tags-list)
- (obsidian--update-all-from-front-matter)

(fsta/measure-time
 (obsidian-update))
;; Was: 18.5s
;; Now: 3.13s

(defun fsta/profile (fn)
  (unwind-protect
      (progn
        (profiler-start 'cpu+mem)
        (funcall fn)
        (profiler-report))
    (profiler-stop)))

(fsta/profile 'obsidian-reset-cache)

(fsta/profile 'obsidian-list-all-tags)

676,921,053  50%             + obsidian-update-tags-list
618,216,897  45%             + obsidian--update-all-from-front-matter
43,563,776   3%             + obsidian-reset-cache


(obsidian-list-all-tags)

(obsidian-find-tags-in-file)


737,324,375  99%            - obsidian-list-all-tags
737,324,375  99%             - let
737,026,415  99%              - mapcar
736,882,783  99%               - #<lambda 0x1fc01655f27d9e>
736,863,775  99%                - mapcar
732,547,556  98%                 - obsidian-find-tags
660,683,918  89%                  - obsidian-read-file-or-buffer
660,683,918  89%                   - if
569,937,263  76%                    + let
90,583,530  12%                      and
71,408,502   9%                  + let
4,000,874   0%                 + function
16   0%               - obsidian-list-all-files



;; Can projectile do the job faster?

(fsta/measure-time
 (obsidian-clear-cache)
 (obsidian-reset-cache)
 (length obsidian-files-cache))
;; 3.44s

(fsta/measure-time
 (projectile-project-files obsidian-directory))
;; => 1.815184s

;; => YES

(fsta/measure-time
 (projectile-dir-files-alien obsidian-directory))
;; 0.17s

(fsta/measure-time
 (let ((dir obsidian-directory))
   (mapcar (lambda (f)
             (file-relative-name (file-name-concat dir f)
                                 obsidian-directory))
           (projectile-dir-files dir))))
;; 1.77s


(fsta/profile
 (lambda ()
   (obsidian-reset-cache)
   (length obsidian-files-cache)))

(fsta/measure-time
 (obsidian-reset-cache)
 (length obsidian-files-cache))

;; (= 120 (- 11901 11781))



(fsta/measure-time
 (obsidian--update-all-from-front-matter))
;; 3.49s
;; Now 1.9 (removed the double "with-temp-buffer")

(fsta/profile
 'obsidian--update-all-from-front-matter)


655,795,893  99%                         - obsidian--file-front-matter
655,585,749  99%                          - let*
558,255,616  84%                           - let
554,943,447  83%                            + save-current-buffer
97,107,050  14%                           + if


;; Most of the time is spent in "save-current-buffer" which is used by
;; "with-temp-buffer"



(obsidian--file-front-matter (file-name-concat obsidian-directory "docs/intro.md"))

(let ((file (file-name-concat obsidian-directory "docs/intro.md")))
  (with-temp-buffer
    (insert-file-contents file)
    (list (point-min) (point-max))))


;;; Another possible optimization: read the files only once for the
;;; front matter and the tags.


;;; if a file is already in the cache, then it must be obsidian-file-p
;;; already
