
;;; Code:
(defun note/get-tags ()
  "Get all the existing tags."
  (interactive)
  ;; TODO use org-roam
  )

;; TODO this is wrong, the org-mode's tag format is :tag1:tag2: etc
(defun note/insert-tags ()
  (interactive)
  (let* ((candidates (note/get-tags)))
    ;; Don't process quit immediatly
    (let ((inhibit-quit t))
      (with-local-quit
        (end-of-line)
        (loop for tag = (completing-read "Tag: " candidates)
              while (and tag (not (string-empty-p tag)))
              do (insert " " tag)))
      ;; Cancel the quit
      (setf quit-flag nil))))

(defun note/infer-title-from-file-name (&optional file-name)
  (let ((parts (split-string
                (file-name-base
                 (file-name-sans-extension
                  (or file-name
                      buffer-file-name)
                  ;; "/home/test/some-thing-dot.org"
                  ))
                "-")))
    (string-join `(,(capitalize (cl-first parts)) ,@(rest parts))
                 " ")))

;; (note/infer-title-from-file-name "some-snake-case-title")
;; => "Some snake case title"
;; (note/infer-title-from-file-name)
;; => "Notes"

(add-hook 'find-file-hook 'auto-insert)
(eval-after-load 'autoinsert
  '(define-auto-insert
     '(org-mode . "org notes skeleton")
     '(""
       "#+title: " (skeleton-read "Title: "
                                  (note/infer-title-from-file-name))
       \n
       "#+tags:" (note/insert-tags))))

;; Remove org-mode auto-inserts
;; (setq auto-insert-alist
;;       (loop for entry in auto-insert-alist
;;       unless (and (listp (car entry))
;;       (eq 'org-mode (caar entry)))
;;       collect entry))
