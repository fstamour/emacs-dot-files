
;; Current value:
slime-contribs
(slime-fancy)

(require 'slime-banner)
(setq slime-contribs
      '(slime-fancy slime-banner))


(setf slime-header-line-p t)

(slime-startup-message)

slime-repl-shortcut-table
(#s(slime-repl-shortcut nil ("restart-inferior-lisp") slime-restart-inferior-lisp "Restart *inferior-lisp* and reconnect SLIME.")
   #s(slime-repl-shortcut slime-repl-compile-and-load ("compile-and-load" "cl") (lambda (filename) (interactive (list (expand-file-name (read-file-name "File: " nil nil nil nil)))) (slime-save-some-lisp-buffers) (slime-repl-shortcut-eval-async (cons 'swank:compile-file-if-needed (cons (slime-to-lisp-filename filename) '(t))) #'slime-compilation-finished)) "Compile (if neccessary) and load a lisp file.")
   #s(slime-repl-shortcut slime-repl-defparameter ("defparameter" "!") (lambda (name value) (interactive (list (slime-read-symbol-name "Name (symbol): " t) (slime-read-from-minibuffer "Value: " "*"))) (insert "(cl:defparameter " name " " value " \"REPL generated global variable.\")") (slime-repl-send-input t)) "Define a new global, special, variable.")
   #s(slime-repl-shortcut slime-repl-quit ("quit") (lambda nil (interactive) (let ((repl-buffer (slime-output-buffer))) (slime-quit-lisp) (kill-buffer repl-buffer))) "Quit the current Lisp.")
   #s(slime-repl-shortcut slime-repl-sayoonara ("sayoonara") (lambda nil (interactive) (if (slime-connected-p) (progn (slime-quit-lisp))) (slime-kill-all-buffers)) "Quit all Lisps and close all SLIME buffers.")
   #s(slime-repl-shortcut slime-repl-disconnect-all ("disconnect-all") slime-disconnect-all "Disconnect all connections.")
   #s(slime-repl-shortcut slime-repl-disconnect ("disconnect") slime-disconnect "Disconnect the current connection.")
   #s(slime-repl-shortcut slime-repl-resend ("resend-form") (lambda nil (interactive) (insert (car slime-repl-input-history)) (insert "
") (slime-repl-send-input)) "Resend the last form.")
   #s(slime-repl-shortcut slime-repl-pop-package ("pop-package" "-p") (lambda nil (interactive) (if (null slime-repl-package-stack) (message "Package stack is empty.") (slime-repl-set-package (car-safe (prog1 slime-repl-package-stack (setq slime-repl-package-stack (cdr slime-repl-package-stack))))))) "Restore the last saved package.")
   #s(slime-repl-shortcut slime-repl-push-package ("push-package" "+p") (lambda (package) (interactive (list (slime-read-package-name "Package: "))) (setq slime-repl-package-stack (cons (slime-lisp-package) slime-repl-package-stack)) (slime-repl-set-package package)) "Save the current package and set it to a new one.")
   #s(slime-repl-shortcut nil ("change-package" "!p" "in-package" "in") slime-repl-set-package "Change the current package.")
   #s(slime-repl-shortcut slime-repl-pop-directory ("pop-directory" "-d" "popd") (lambda nil (interactive) (if (null slime-repl-directory-stack) (message "Directory stack is empty.") (slime-set-default-directory (car-safe (prog1 slime-repl-directory-stack (setq slime-repl-directory-stack (cdr slime-repl-directory-stack))))))) "Restore the last saved directory.")
   #s(slime-repl-shortcut slime-repl-push-directory ("push-directory" "+d" "pushd") (lambda (directory) (interactive (list (read-directory-name "Push directory: " (slime-eval '(swank:default-directory)) nil nil ""))) (setq slime-repl-directory-stack (cons (slime-eval '(swank:default-directory)) slime-repl-directory-stack)) (slime-set-default-directory directory)) "Save the current directory and set it to a new one.")
   #s(slime-repl-shortcut nil ("pwd") (lambda nil (interactive) (let ((dir (slime-eval '(swank:default-directory)))) (message "Directory %s" dir))) "Show the current directory.")
   #s(slime-repl-shortcut nil ("change-directory" "!d" "cd") slime-set-default-directory "Change the current directory.")
   #s(slime-repl-shortcut slime-repl-shortcut-help ("help") slime-list-repl-short-cuts "Display the help."))

(slime-repl-set-package)

(mapcar #'slime-repl-shortcut.names slime-repl-shortcut-table)

(slime-list-repl-short-cuts)
