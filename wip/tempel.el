

;; completion-at-point-functions


(defun test-tempel ()
  (interactive)
  (tempel-insert '("┌─" (make-string (length str) ?─) "─┐" n
                   "│ " (s str)                       " │" n
                   "└─" (make-string (length str) ?─) "─┘" n)))


;;; How tempel uses overlays:

(defvar-local tempel--active nil
  "List of active templates.
Each template state is a pair, where the car is a list of overlays and
the cdr is an alist of variable bindings.  The template state is attached
to each overlay as the property `tempel--field'.  Furthermore overlays
may be named with `tempel--name' or carry an evaluatable Lisp expression
`tempel--form'.")

;; (st (overlay-get ov 'tempel--field))
;;  => st stands for "state" (See 'tempel-field)



;; defvar-keymap = defvar + define-keymap
;; see easy-menu-define
;; must see: keymap-set's docstring
;;  - can refer to other keymap
;; see key-valid-p
;;  - [...] the name of an event, surrounded by angle brackets [...]
;; See `kbd' for a descripion of KEYS.
;; See edmacro-mode

(tempel--print-template
 (first
  (tempel-path-templates)))








TODO

FOR lisp mode

put tempel before slime

(let ((first (pop completion-at-point-functions))
      (second (pop completion-at-point-functions)))
  )




(let* ((project-root (projectile-acquire-root))
       (files (projectile-project-files project-root)))
  files)


(fsta/measure-time
 (fsta/terraform/find-all-resources))

(fsta/terraform/complete-resource)



(when (region-active-p)
  (buffer-substring (mark) (point)))

(let ((r "resource \"auth0_role_permissions\" \"role_permission\" {"))
  (string-replace
   "\" {" ""
   (string-replace
    "\" \"" "."
    (string-replace
     "resource \"" "" r))))

(car (split-string "auth0_role_permissions.role_permission" "\\."))
