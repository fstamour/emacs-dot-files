;; Run this once in a while
;; (package-refresh-contents)


(require 'cl-lib) ;; for pushnew

(use-package auto-complete
  :config (global-auto-complete-mode t))





;; helm: incemental completion and selection framework
;; (load "~/.emacs.d/helm.el")

;; alternative to helm: ivy + counsel
(use-package ivy
  :config (ivy-mode 1))

;; (use-package counsel
;;   :bind (("C-s" . 'swiper-isearch))
;;   :config (counsel-mode 1))

;; python-mode
(use-package python
  :mode ("\\.py\\'" . python-mode)
  :interpreter ("python" . python-mode))


;; can be used to tie related commands into a family of short bindings
;; with a common prefix - a Hydra.
(use-package hydra)
;; https://github.com/abo-abo/hydra/wiki/Emacs

;; vim emulation layer
(use-package evil
  :init
  (setq evil-want-keybinding nil)
  :config
  (evil-mode 1)
  ;; Rebind M-. to find-definitions in emacs lisp mode
  (evil-define-key 'normal 'emacs-lisp-mode-map (kbd "M-.") 'xref-find-definitions))

(use-package evil-collection
  :after evil
  :ensure t
  :config
  (evil-collection-init))

;; Slightly more vim-like - increment numbers with C-a, decrement with C-S-a
(use-package evil-numbers :config
  (define-key evil-normal-state-map (kbd "C-a") 'evil-numbers/inc-at-pt)
  (define-key evil-normal-state-map (kbd "C-S-a") 'evil-numbers/dec-at-pt))


;; Doesn't work on windows because of the space in "Program Files"
;; https://emacs.stackexchange.com/a/17890
;; (use-package exec-path-from-shell
;;   :config (progn
;; 	    (exec-path-from-shell-copy-env "SSH_AGENT_PID")
;; 	    (exec-path-from-shell-copy-env "SSH_AUTH_SOCK")))


;; use ssh-agent on windows from emacs
(use-package ssh-agency)

;; syntax checking for emacs
(use-package flycheck
  :config (progn
	    (global-flycheck-mode)
	    (setf flycheck-python-pycompile-executable
		  (executable-find "python"))))

(use-package flyspell)

;; company: Modular in-buffer completion framework for Emacs
;; (use-package company :config (add-hook 'after-init-hook 'global-company-mode))
;; (global-company-mode 0)

(use-package omnisharp)
(use-package powershell)

;; Edit a region in an "indirect" buffer
(use-package edit-indirect)

;; Goto char (it's like vimium's "f")
(use-package avy
  :config (evil-define-key 'normal 'global
	                   "gc" 'avy-goto-char)
  :bind (("C-:" . 'avy-goto-char)))





(defun %customize-evil-map (key-map mapping-spec)
  " Help redefine a bunch of keys in normal mode of a specific major mode
Returns a progn that needs to be evaluated
 Example usage:
(%customize-evil-map 'mode-map '(\"M-.\" 'slime-edit-definition))
;; => (progn (evil-define-key (quote normal) mode-map (kbd \"M-.\") (quote slime-edit-definition)))

(%customize-evil-map
       'elfeed-search-mode-map
       '(\"RET\"		'elfeed-search-show-entry
	 \"y\"		'elfeed-search-yank))\
;; =>
;; (progn (evil-define-key (quote normal) elfeed-search-mode-map (kbd \"y\") (quote elfeed-search-yank))
;;        (evil-define-key (quote normal) elfeed-search-mode-map (kbd \"RET\") (quote elfeed-search-show-entry)))
"
  (let ((plist mapping-spec)
	(result '()))
    (while plist
      (let ((key (car plist))
	    (command (cadr plist)))
	(push
	 (list 'evil-define-key ''normal key-map (list 'kbd key) command)
	 result))
      (setq plist (cddr plist)))
    (append '(progn) result)))


;; Default elfeed-search-mode-map's keybindings
;; '("RET"		'elfeed-search-show-entry
;;  "+"		'elfeed-search-tag-all
;;  "-"		'elfeed-search-untag-all
;;  "<"		'elfeed-search-first-entry
;;  ">"		'elfeed-search-last-entry
;;  "G"		'elfeed-search-fetch
;;  "S"		'elfeed-search-set-filter
;;  "b"		'elfeed-search-browse-url
;;  "c"		'elfeed-search-clear-filter
;;  "g"		'elfeed-search-update--force
;;  "h"		'describe-mode
;;  "n"		'next-line
;;  "p"		'previous-line
;;  "q"		'elfeed-search-quit-window
;;  "r"		'elfeed-search-untag-all-unread
;;  "s"		'elfeed-search-live-filter
;;  "u"		'elfeed-search-tag-all-unread
;;  "y"		'elfeed-search-yank)

;; Default elfeed-show-mode-map's keybindings
;; '("TAB"		'elfeed-show-next-link
;;  "SPC"		'scroll-up-command
;;  "+"		'elfeed-show-tag
;;  "-"		'elfeed-show-untag
;;  "<"		'beginning-of-buffer
;;  ">"		'end-of-buffer
;;  "A"		'elfeed-show-add-enclosure-to-playlist
;;  "P"		'elfeed-show-play-enclosure
;;  "b"		'elfeed-show-visit
;;  "c"		'elfeed-kill-link-url-at-point
;;  "d"		'elfeed-show-save-enclosure
;;  "g"		'elfeed-show-refresh
;;  "h"		'describe-mode
;;  "n"		'elfeed-show-next
;;  "p"		'elfeed-show-prev
;;  "q"		'elfeed-kill-buffer
;;  "s"		'elfeed-show-new-live-search
;;  "u"		'elfeed-show-tag--unread
;;  "y"		'elfeed-show-yank)

(use-package elfeed
  :config
  (progn
    (eval-after-load 'evil-core
      `(progn
	 ,(%customize-evil-map
	   'elfeed-search-mode-map
	   '("RET"		'elfeed-search-show-entry
	     "+"		'elfeed-search-tag-all
	     "-"		'elfeed-search-untag-all
	     "<"		'elfeed-search-first-entry
	     ">"		'elfeed-search-last-entry
	     "G"		'elfeed-search-fetch
	     "S"		'elfeed-search-set-filter
	     "b"		'elfeed-search-browse-url
	     "c"		'elfeed-search-clear-filter
	     "j"		'next-line
	     "k"		'previous-line
	     "q"		'elfeed-search-quit-window
	     "r"		'elfeed-search-untag-all-unread
	     "s"		'elfeed-search-live-filter
	     "u"		'elfeed-search-tag-all-unread
	     "y"		'elfeed-search-yank))
	 ,(%customize-evil-map
	   'elfeed-show-mode-map
	   '("TAB"		'elfeed-show-next-link
	     "+"		'elfeed-show-tag
	     "-"		'elfeed-show-untag
	     "A"		'elfeed-show-add-enclosure-to-playlist
	     "P"		'elfeed-show-play-enclosure
	     "b"		'elfeed-show-visit
	     "c"		'elfeed-kill-link-url-at-point
	     "d"		'elfeed-show-save-enclosure
	     "M-n"		'elfeed-show-next
	     "M-p"		'elfeed-show-prev
	     "q"		'elfeed-kill-buffer
	     "s"		'elfeed-show-new-live-search
	     "u"		'elfeed-show-tag--unread
	     "y"		'elfeed-show-yank))))
    (setq elfeed-feeds '("https://lobste.rs/rss"))))

;; Mail
;; (use-package mu4e)

(when (executable-find "fish")
  (use-package fish-completion
    :config
    (global-fish-completion-mode)))

(use-package hl-todo :config (global-hl-todo-mode 1))

(use-package window-numbering
  :config (window-numbering-mode 1))

(use-package gitlab-ci-mode)

(use-package plantuml-mode
  :config
  (let ((plantuml (executable-find "plantuml"))
	(dot (executable-find "dot")))
    (when plantuml
      (setq plantuml-jar-path (expand-file-name
			       (concat (file-name-directory plantuml)
				       "../lib/plantuml.jar"))
	    plantuml-default-exec-mode 'jar)
      (when dot
	(setq plantuml-jar-args (list "-graphvizdot" dot "-charset" "UTF-8"))))))

;; (plantuml-enable-debug)

;; https://github.com/smihica/emmet-mode
(use-package emmet-mode
  :config
  (progn
    (add-hook 'sgml-mode #'emmet-mode)
    (add-hook 'css-mode #'emmet-mode)))


(use-package dockerfile-mode
  :config
  (add-to-list 'auto-mode-alist '("Dockerfile\\'" . dockerfile-mode)))

(use-package ace-window
  :config
  (global-set-key (kbd "M-o") 'ace-window))

;; Display availble bindings
(use-package which-key
  :config
  (which-key-mode 1))



;; "nice" writing environment
(use-package olivetti)
