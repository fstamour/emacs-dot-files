;;; html.el --- configuration for html editing       -*- lexical-binding: t; -*-

;; Copyright (C) 2020  

;; Author:  <Francis St-Amour>
;; Keywords: abbrev

(define-auto-insert
  '(html-mode . "html mode skeleton pour des recettes")
  '(""
    > "<html>" \n
    > "<head>" \n
    > "<title>" (skeleton-read "Title: ") "</title>" \n
    > \n
    > "<meta charset=\"utf-8\" />" \n
    > "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=yes\" />" \n
    > \n
    > "<link rel=\"stylesheet\" href=\"../tufte/tufte.css\" />" \n
    > "</head>" \n
    > "<body>" \n
    > "<h1></h1>" \n
    > \n
    > "<table>" \n
    > "<tr><td>Préparation</td><td> Min</td></tr>" \n
    > "<tr><td>Cuisson</td><td> Min</td></tr>" \n
    > "<tr><td>Portions</td><td></td></tr>" \n
    > "</table>" \n
    > \n
    > "<h2>Ingrédients</h2>" \n
    > "<ul>" \n
    > "</ul>" \n
    > \n
    > "<h2>Étapes</h2>" \n
    > "<ol>" \n
    > \n
    > "<li>Lorem" \n
    > "<ol>" \n
    > "<li>Ipsum</li>" \n
    > "</ol>" \n
    > "</li>" \n
    > "</ol>" \n
    > \n
    > "<h2>Notes</h2>" \n
    > \n
    > "<p>Lorem ipsum</p>" \n
    > \n
    > \n
    > "<h2>Sources</h2>" \n
    > "<a href=\"\"></a>" \n
    > \n
    > "</body>" \n
    > "</html>" \n))
