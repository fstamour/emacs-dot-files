

darkroom-mode
- installed with guix home
- gives me "arithmetic error" and the text is not centered

focus-mode


olivetti
zen-mode
writeroom-mode


;; https://github.com/joostkremers/writeroom-mode
;; installed with guix-home
writeroom-major-modes



;; use =M-x scroll-lock-mode= to keep the cursor centered

Themes:

(setq custom-safe-themes t)

I liked base16-atelier-forest, but some things (like the values in customize-variables) are too low contrast (gray on gray). I love the parens' colours

base16-atelier-cave
base16-atelier-dune

base16-ayu-dark - needs higher contrast for comments and region

base16-bespin seems good!

base16-blueforest ♥

base16-blueish not bad
base16-brewer not bad
base16-catppuccin-macchiato not bad

base16-brogrammer - funny


base16-chalk > base16-catppuccin ♥
base16-classic-dark


base16-colors

base16-da-one-black


base16-darkviolet

base16-evenok-dark

base16-everforest

base16-gigavolt


abyss

(customize-themes)
