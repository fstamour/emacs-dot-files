

(defun fsta/open-in-vscode ()
  (interactive)
  (when-let ((filename (buffer-file-name (current-buffer))))
    (shell-command (format "code \"%s\"" filename))))
