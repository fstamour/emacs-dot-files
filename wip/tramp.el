;; Trying to make tramp work on windows

(defvar username "REPLACEME")

(setq tramp-default-method "ssh")

;; (find-file (format "/ssh:%s@10.0.103/" username)) ; wrong syntax

(find-file (format "/ssh:%s@mu:.bashrc" username))

(executable-find "ssh")


(let ((ssh (fsta/slurp-file "~/../../.ssh/environment")))
  (list
   (and ssh
        (string-match "SSH_AUTH_SOCK[=\s]\\([^\s;\n]*\\)" ssh)
        (setenv       "SSH_AUTH_SOCK" (match-string 1 ssh)))
   (and ssh
        (string-match "SSH_AGENT_PID[=\s]\\([0-9]*\\)?" ssh)
	(setenv       "SSH_AGENT_PID" (match-string 1 ssh)))))

(file-exists-p)

(expand-file-name
 (concat
  (file-name-directory
   (executable-find "git"))
  "../usr/bin/ssh.exe"))

(setf (alist-get 'tramp-login-program
		 (alist-get "ssh"
			    tramp-methods nil nil #'string=))
      ;; default
      ;; '("ssh")
      ;; git's ssh
      (list
       (expand-file-name
	(concat
	 (file-name-directory
	  (executable-find "git"))
	 "../usr/bin/ssh.exe")))
      )

(setf tramp-shell-prompt-pattern.orig tramp-shell-prompt-pattern) ;; save
(setf tramp-shell-prompt-pattern tramp-shell-prompt-pattern.orig) ;; restore

(setf tramp-shell-prompt-pattern
      "^[^$]*\\> ")

(setq tramp-debug-buffer t)
(setq tramp-verbose 10)

(alist-get "sshx"
	   tramp-methods nil nil #'string=)


(setf shell-file-name.orig shell-file-name) ;; save
(setf shell-file-name shell-file-name.orig) ;; restore

(setf shell-file-name
      (expand-file-name
       (concat
	(file-name-directory
	 (executable-find "git"))
	"../usr/bin/bash.exe")))


"c:/Git/usr/bin/bash.exe"

;; (setf debug-on-error t) ; was already t
