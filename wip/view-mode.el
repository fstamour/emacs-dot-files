
(defun view-indirect-buffer-other-window ()
  (interactive)
  (clone-indirect-buffer-other-window (buffer-name) t)
  (view-buffer (current-buffer)))
