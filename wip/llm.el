;; code completion

;; Doctring, and comments

;; Generate commit messages from diff

;; Somehow help with code reviews?

;; Generate unit tests

;; org-mode + literate (might be useful when investigating stuff)


;; hello.c
;; bool is_prime(int x) {

;; C-c C-k


(defvar copilot-llamafile
  (expand-file-name "~/dev/llm/wizardcoder-python-13b.llamafile"))

(setf copilot-llamafile
      (expand-file-name "~/dev/llm/phi-2.Q5_K_M.llamafile"))

(file-exists-p copilot-llamafile)
(file-executable-p copilot-llamafile)



(cl-defmacro copilot--with-temp-file ((var prefix) &body body)
  `(let ((,var (make-temp-file ,prefix)))
     (unwind-protect
         (progn ,@body)
       ;; (delete-file ,var)
       )))

;; (copilot--with-temp-file (stderr "emacs-copilot-stderr.") stderr)



(make-variable-buffer-local 'copilot-id)

(defun copilot--setup-id ()
  (unless (and (boundp 'copilot-id) copilot-id)
    (set (make-local-variable 'copilot-id) (make-temp-name ""))))

(defun copilot--infer-lang ()
  (string-remove-suffix "-mode" (symbol-name major-mode)))

(defun copilot--prompt (cache)
  "Create new prompt for this interaction."
  (let* ((point (point))
         (lang (copilot--infer-lang))
         ;; extract current line, to left of caret
         ;; and the previous line, to give the llm
         (code (save-excursion
                 (dotimes (i 2)
                   (when (> (line-number-at-pos) 1)
                     (forward-line -1)))
                 (beginning-of-line)
                 (buffer-substring-no-properties (point) point)))
         (system "\
You are an Emacs code generator. \
Writing comments is forbidden. \
Writing test code is forbidden. \
Writing English explanations is forbidden. ")
         (prompt (format
                  "[INST]%sGenerate %s code to complete:[/INST]\n```%s\n%s"
                  (if (file-exists-p cache) "" system) lang lang code)))
    prompt))

(defun copilot--rewrite-history (hist)
  ;; iterate text deleted within editor then purge it from prompt
    (when kill-ring
      (save-current-buffer
        (find-file hist)
        (dotimes (i 10)
          (let ((substring (current-kill i t)))
            (when (and substring (string-match-p "\n.*\n" substring))
              (goto-char (point-min))
              (while (search-forward substring nil t)
                (delete-region (- (point) (length substring)) (point))))))
        (save-buffer 0)
        (kill-buffer (current-buffer)))))

(defun copilot-complete ()
  (interactive)
  (copilot--setup-id)
  (let* ((point (point))
         (inhibit-quit t)
         (curfile (or (buffer-file-name) (buffer-name)))
         (tmp-base (file-name-concat temporary-file-directory copilot-id))
         (cache (concat tmp-base ".cache"))
         (hist (concat tmp-base ".prompt"))
         (prompt (copilot--prompt cache)))


    (write-region "" nil cache 'append 'silent)

    (copilot--rewrite-history hist)

    ;; append prompt for current interaction to the big old prompt
    (write-region prompt nil hist 'append 'silent)

    ;; run llamafile streaming stdout into buffer catching ctrl-g
    (copilot--with-temp-file
     (stderr "emacs-copilot-stderr.")
     (message "Prompt: %S" prompt)
     (with-local-quit
       (call-process
        ;; Program
        copilot-llamafile
        ;; infile (nil means /dev/null)
        nil
        ;; ;; destination  1> current-buffer 2> /dev/null
        ;; (list (current-buffer) nil)
        (list (current-buffer) stderr)
        ;; Whether to redisplay the buffer as output is inserted
        t
        "--cli" ;; just making sure
        "--prompt" prompt
        ;; "--prompt-cache" cache
        ;; "--prompt-cache-all"
        "--silent-prompt"
        ;; "--temp" "0"
        "-c" "1024"
        ;; "-ngl" "35"
        ;; "-r" "```"
        ;; "-r" "\n}"
        ;; "-f" hist
        )))

    ;; get rid of most markdown syntax
    (let ((end (point)))
      (save-excursion
        (goto-char point)
        (while (search-forward "\\_" end t)
          (backward-char)
          (delete-char -1 nil)
          (setq end (- end 1)))
        (goto-char point)
        (while (search-forward "```" end t)
          (delete-char -3 nil)
          (setq end (- end 3))))

      ;; append generated code to prompt
      (write-region point end hist 'append 'silent))
    (message "copilot-complete DONE")))
