
;; use-package is not part of emacs 28, only > 29
;; (setq use-package-always-ensure nil)
;; (use-package sly)



;; I was trying to debug issues with emacs packages installed with
;; guix


(cl-remove-duplicates
 (string-split
  (getenv "EMACSLOADPATH")
  ":")
 :test 'string=)

(setenv "EMACSLOADPATH"
        (concat
         (getenv "EMACSLOADPATH")
         ":"
         (getenv "HOME") "/.guix-profile/share/emacs/site-lisp"))

(add-to-list 'load-path
             (concat (getenv "HOME")
                     "/.guix-profile/share/emacs/site-lisp"))

(add-to-list 'load-path
             (concat (getenv "HOME")
                     "/.guix-profile/share/emacs/site-lisp/darkroom-0.3"))

(require 'guix-emacs)


(require 'darkroom)

;; (getenv "GUIX_PROFILE") /share/emacs/site-lisp

(require 'sly)

;; (pop load-path)

(guix-emacs-autoload-packages)

;; load-path





geiser-guile-binary
"/gnu/store/bhynhk0c6ssq3fqqc59fvhxjzwywsjbb-guile-3.0.9/bin/guile"

(setf geiser-guile-binary
      (expand-file-name "~/dev/guix-configurations/repl"))
