
(defun highlight-regexp-all-windows (regexp &optional face subexp lighter)
  "Toggle highlighting of the symbol at point in all windows."
  (interactive
   (list
    (hi-lock-regexp-okay
     (read-regexp "Regexp to highlight"
                  (if (use-region-p)
                      (prog1
                          (buffer-substring (region-beginning)
                                            (region-end))
                        (deactivate-mark))
                    'regexp-history-last)))
    (hi-lock-read-face-name)
    current-prefix-arg))
  (save-selected-window
    (cl-dolist (x (window-list))
      (select-window x)
      (highlight-regexp regexp))))
