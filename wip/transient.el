
;;;
;;; Prefix
;;;

(transient-define-prefix fsta/my-first-lil-transient ()
  "Trying out magit's transient"
  :info-manual "Trying out magit's transient"
  ["Group 1"
   ;; TODO Test this: :if feels-like-it-p
   [("p" "Print the arguments"   fsta/my-first-lil-transient/hi)
    ""                            ; it's possible to add an empty line
    "^^^Empty row above^^^"
    ("P" "Print the arguments"   fsta/my-first-lil-transient/hi)]
   [("p" "Row 1 Column 2"   fsta/my-first-lil-transient/hi)
    " Look, it's possible to print something in a row"
    ("P" "Row 2 Column 2"   fsta/my-first-lil-transient/hi)]]

  ["Arguments"
   [("-s" "SCREAM" "--scream")
    ;; ("-n" "Provide a name")
    (fsta/transient-option/name)
    (fsta/transient-option/colour)]]

  ["Essential commands" ("q" "Cancel" transient-quit-one)])

;;;
;;; Infix
;;;

;; In this case, transient-define-infix is a macro to create a
;; transient-option structure.
(transient-define-infix fsta/transient-option/name ()
  :description "Optional name"
  :class 'transient-option
  :shortarg "-n"
  :argument "--name=")

(transient-define-infix fsta/transient-option/colour ()
  :description "Optional colour"
  :class 'transient-option
  :shortarg "-c"
  :argument "--colours="
  :choices '("red" "blue" "green"))

;; Use :class 'transient-switch e.g. for -p

;;;
;;; Commands (are those suffixes?)
;;;

(defun fsta/my-first-lil-transient/hi (&rest args)
  (interactive (list (transient-args 'fsta/my-first-lil-transient)))
  (print args)
  ;; (if (member "--scream" args) ...)
  ;;
  )


#|

Perhaps don't rely on multiple columns because transient-force-single-column is a thing.

> Note that an infix is a special kind of suffix. Depending on context “suffixes” means “suffixes (including infixes)” or “non-infix suffixes”.

https://magit.vc/manual/transient/Defining-Transients.html
https://magit.vc/manual/transient/Group-Specifications.html
https://magit.vc/manual/transient/Suffix-Specifications.html
https://magit.vc/manual/transient/Predicate-Slots.html

|#
