
(switch-to-buffer-other-window "*svg*")

(let* ((buffer (get-buffer-create "*svg*")) ; create the buffer
       (canvas-scale 8.0)
       (w (* canvas-scale 100))
       (h (* canvas-scale 100))
       (svg (svg-create w h))
       (divisions 10.0)
       (step (/ w divisions)))		; create the svg
  ;; Draw the svg

  (svg-rectangle svg 0 0 w h :stroke "black" :fill "white" :stroke-width 1)

  (cl-loop for i from 0 to divisions
	   for sum = (* i step)
	   do
	   (svg-line svg sum 0 sum h :stroke "black")
	   (svg-line svg 0 sum w sum :stroke "black"))

  ;; Save to file
  (fsta/comment
   (with-temp-file "d:/grid.svg"
     (set-buffer-multibyte nil)
     (svg-print svg)))

  ;; Insert the svg in the buffer
  (with-current-buffer buffer
    (erase-buffer)
    (insert-image (svg-image svg :scale 1.0 :ascent 'center))))
