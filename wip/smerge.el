
(defun fsta/ensure-indirect-buffer (suffix)
  (interactive "SSuffix of the indirect buffer: ")
  (let* ((newname (format "%s<%s>" (buffer-name) suffix))
         (buffer (get-buffer newname)))
    (if buffer
        ;; already exists
        (if (eq (buffer-base-buffer buffer) (current-buffer))
            buffer ; return the existing buffer
          (error "The buffer named %S already exists, but is not an indirect buffer of %S." (buffer-name)))
      ;; doesn't exist already
      (clone-indirect-buffer-other-window newname t))))

;; Once in a while I get a huge conflict where the "upper" and "base"
;; version of the code are long and extremely similar, I'm trying to
;; "reduce" the differences to actual differences.
(defun fsta/smerge-reduce ()
  (interactive)
  (smerge-match-conflict)
  ;; The submatches contain:
  ;; 0:  the whole conflict.
  ;; 1:  upper version of the code.
  ;; 2:  base version of the code.
  ;; 3:  lower version of the code.
  ;; An error is raised if not inside a conflict.
  (let* ((original-buffer (current-buffer))
         (buffer-upper (prog1 (fsta/ensure-indirect-buffer "upper")
                         (narrow-to-region )))
         (buffer-base (save-excursion (with-current-buffer (current-buffer)
                                        (fsta/ensure-indirect-buffer "base"))))))
  )

(clone-indirect-buffer-other-window)
(with-current-buffer "upper"
  (erase-buffer)
  (insert (fsta/buffer-substring-match 1)))
(with-current-buffer "upper"
  (fsta/buffer-substring-match 1))


(buffer-base-buffer)


...nevermind, I can use `smerge-diff-base-upper'...
