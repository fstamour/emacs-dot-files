(makunbound 'emacs-configuration-org-mode-map)

(let ((beg "#+begin_src emacs-lisp")
      (end "#+end_src"))
  (define-minor-mode emacs-configuration-org-mode
    "A minor mode especially crafted for the current file."
    :init-value t
    :lighter " ecom"
    :keymap
    `(
      ;; Insert a new block
      ;; TODO Check if the region is active, surround it instead
      ([f5] . (lambda ()
		(interactive)
		(insert ,beg)
		(newline)
		(newline)
		(insert ,end)
		))
      ;; Split a block
      ([f6] . (lambda ()
		(interactive)
		(insert ,end)
		(newline)
		(newline)
		(insert ,beg)
		)))))
