(require 'dape)

;; forcing emacs to load straight from the .el file, because
;; straight's developpers are smart-asses and added a check to make
;; sure the emacs version is the same between the emacs that compiled
;; the elc file and the emacs that loaded it.
(load (find-library-name "straight"))

(straight-use-package 'dape)

(straight-use-package
 '(el-patch :type git :host github :repo "svaante/dape"))

;; this should load ~/.emacs.d/straight/repos/dape/dape.el
(require 'dape)


(straight-use-package
 '(el-patch :type git :host github :repo "svaante/dape"))

(require 'jsonrpc)
