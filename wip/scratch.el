;;; package -- scratch

;;; Commentary:
;;
;; I try stuff here
;;
;;; Code:

(require 'cl)
(require 'evil) ;; Actually optional

(defvar scratch-temporary-buffers '())
(defvar scratch-mode-map (make-sparse-keymap))

(defun scratch-make-temporary-buffer ()
  "Make a buffer name, add it to the list."
  (let ((buffer-name (make-temp-name (concat "tmp-" (format-time-string "%Y-%m.%dT%H.%M.%S-")))))
    (push buffer-name scratch-temporary-buffers)
    buffer-name))

(defun scratch-kill-all-temporary-buffer ()
  "Kill all buffers."
  (interactive)
  (dolist (buffer-name scratch-temporary-buffers)
    (when (get-buffer buffer-name)
      (kill-buffer buffer-name)))
  (setf scratch-temporary-buffers '()))


;; Assumes evil is already loaded
(progn
  (define-key scratch-mode-map (kbd "q") 'kill-current-buffer)
  (evil-define-key 'normal 'scratch-mode-map (kbd "q") 'kill-current-buffer))

(cl-defun scratch-choose-and-call-command (prompt command-alist &key allow-other-p)
  "Let the user choose a command to run from a list."
  (let* ((choice (completing-read prompt (mapcar 'car command-alist)))
	 (command (or (cdr (assoc choice command-alist))
		      (and allow-other-p choice))))
    (when command
      (call-interactively command))))

(defun scratch-choose-major-mode ()
  "Choose a major mode interactively."
  (interactive)
  (scratch-choose-and-call-command
   "Choose a major mode: "
   '(("lisp" . slime-mode))
   :allow-other-p t))
     

(defun scratch-insert ()
  "choose an action to start to do someting"
  (interactive)
  ;; TODO Filter the choices based on the context
  (scratch-choose-and-call-command
   "What do you want to insert? "
   '(("defpackage scratch")
     ("defpackage" . scratch-insert-defpackage)
     ("defun" . scratch-insert-defun)
     ("defvar" . scratch-insert-defvar)
     ("defparameter" . scratch-insert-defparameter)
     ("let" . scratch-insert-let)
     ("defmacro" . scratch-insert-defmacro)
     ;; TODO
     ;; defclass
     ;; slots
     ;; defgeneric
     ;; defmethod
     )))

(defun scratch-new-buffer ()
  (interactive)
  (switch-to-buffer-other-window
   (scratch-make-temporary-buffer))
  (scratch-mode)
  (scratch-choose-major-mode))



(define-minor-mode scratch-mode
  "Scratch mode."
  :lighter " sctch"
  :keymap scratch-mode-map)

(provide 'scratch)
;;; end of scratch.el
