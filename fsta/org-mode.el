;; Autofill
(add-hook 'org-mode-hook 'turn-on-auto-fill)

;; I don't want to indent the nodes' content with the headline
(setq org-adapt-indentation nil)

;; Navigation
(setq org-goto-interface 'outline-path-completion)
(setq org-goto-max-level 10)
(setq org-outline-path-complete-in-steps nil)


;;; Customizing org-refile

(setf org-refile-use-outline-path 'file
      org-outline-path-complete-in-steps nil
      org-refile-allow-creating-parent-nodes 'confirm)

(defun fsta/list-sibbling-org-files ()
  "List \".org\" files in the same directory."
  ;; currently assumes that the current buffer is backed by a file.
  (directory-files (file-name-directory buffer-file-name)
                   t
                   "^[^.].*\.org$"))

(defun fsta/find-org-mode-buffers ()
  "Find all buffer whose's file-name ends with \".org\"."
  (cl-loop for buffer in (buffer-list)
           for name = (buffer-file-name buffer)
           when (and name (string-match "\\.org$" name))
           collect name))

;; TODO generate a safe filename from the current heading (begind refiled)
;; This is a hack to move headings to a new file more easily...
(defun fsta/heading-to-filename ()
  (message "fsta/heading-to-filename: %S" (org-get-outline-path))
  nil)

(setf org-refile-targets
      '((nil :maxlevel . 9)
        (fsta/list-sibbling-org-files :maxlevel . 3)
        (fsta/find-org-mode-buffers :maxlevel . 3)
        (fsta/heading-to-filename :maxlevel . 1)))

;; Another candidate..
;; (org-agenda-files :maxlevel . 3)

;; TODO Try (let ((org-archive-location ...) (org-archive-save-context-info nil)))
;; e.g. it might be possible to abuse org-archive to replace org-refile in some cases



;; TODO WIP - This might be useful to automate some stuff...
(defun fsta/ensure-heading (heading)
  "Add the top-level heading HEADING if it doesn't already exists."
  (let* ((rx-to-string `(seq line-start ,(concat "* " heading))))
    (unless (re-search-forward heading nil t)
      (goto-char (point-max))
      (insert "* " heading "\n\n"))))


;;; Agenda

(require 'org-agenda)

;; per-host agenda file
(cl-pushnew (expand-file-name "~/agenda.org") org-agenda-files :test 'string=)
