;; This package adds nice interfaces to list and manipulate docker
;; images, containers and much more.
(use-package docker :config)

(use-package dockerfile-mode)

;; TODO add "docker" to 'treesit-language-source-alist
;; (add-to-list 'auto-mode-alist '("\\.dockerfile\\'" . docker-ts))
;; (add-to-list 'auto-mode-alist '("[/\\]\\(?:Containerfile\\|Dockerfile\\)\\(?:\\.[^/\\]*\\)?\\'" . dockerfile-ts-mode) )
