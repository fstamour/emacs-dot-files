;; https://github.com/radian-software/prescient.el

;; to make sorting and filtering more intelligent
(use-package vertico-prescient
  :after vertico
  :config
  (vertico-prescient-mode 1)
  ;; to save your command history on disk, so the sorting gets more
  ;; intelligent over time
  (prescient-persist-mode 1))
