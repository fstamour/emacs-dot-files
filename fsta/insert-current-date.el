;; Insert date
(defun fsta/insert-current-date ()
  "Insert an ISO-8601 data at point."
  (interactive)
  ;; Just like ISO-8601
  (insert
   (format-time-string "%Y-%m-%d")))
