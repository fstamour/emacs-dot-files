(use-package ace-window
  :config
  (setf
   ;; Use the arrow keys instead of numbers
   aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l)
   ;; Scope to the current frame only
   aw-scope 'frame))

;; (define-key fstamour-minor-mode-map [M-o] 'ace-window)
