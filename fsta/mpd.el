;; https://ideatrash.net/2016/08/volume-normalization-made-easy-with-mpd.html
;; https://ideatrash.net/2020/06/weekend-project-whole-house-and-streaming-audio-for-free-with-mpd.html
;; https://wiki.archlinux.org/title/Music_Player_Daemon

(use-package simple-mpc)
(simple-mpc)

;; requires mpc exectuable



;; apparently emacs comes with mpc.el (by Stefan Monnier <monnier@iro.umontreal.ca>)

(require 'mpc)
(setq mpc-host "nu")
(mpc)
(mpc-quit)

;; steep learning curve?



;; https://github.com/mpdel/mpdel
;; uses libmpdel
(use-package mpdel)
(setq libmpdel-hostname "nu")
(mpdel-browser-open)

;; elmpd
;; mpdel
;; ivy-mpdel
;; mpdel-embark
;; mpdmacs
;; mingus

;; libmpdee
;; libmpdel
