
(use-package terraform-mode
  :ensure t
  :custom (terraform-format-on-save t))

(add-hook 'terraform-mode-hook 'eglot-ensure)

;; find-all-data?
;; locals?

(defun fsta/terraform/find-all-resources ()
  (split-string
   (shell-command-to-string
    (format "rg '^resource' %s  --no-filename --no-heading | sed 's/^resource \"//; s/\" \"/./; s/\".*$//'" (projectile-acquire-root)))
   "\n"))

(defun fsta/terraform/complete-resource ()
  (interactive)
  (completing-read "Choose a resource: "
                   (fsta/terraform/find-all-resources)
                   nil ; predicate
                   t ; require-match
                   ;; initial-input
                   nil))
