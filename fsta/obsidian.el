
(use-package obsidian
  ;; I use my own fork, because I was trying to improve that package.
  :load-path "fsta/obsidian/"
  :demand t ; force to load immediately
  :config
  (obsidian-specify-path
   (if (member system-name '("phi" "nu"))
       "~/notes/"
     "~/dev/wiki/"))
  ;; Automatically enable obsidian-mode if a markdown file is part of
  ;; an obisdian vault.
  (global-obsidian-mode t)
  :custom
  ;; This directory will be used for `obsidian-capture' if set.
  (obsidian-inbox-directory "Inbox")
  ;; Create missing files in inbox? - when clicking on a wiki link
  ;; t: in inbox, nil: next to the file with the link
  ;; default: t
                                        ;(obsidian-wiki-link-create-file-in-inbox nil)
  ;; The directory for daily notes (file name is YYYY-MM-DD.md)
  ;; (obsidian-daily-notes-directory "Daily Notes")
  ;; Directory of note templates, unset (nil) by default
                                        ;(obsidian-templates-directory "Templates")
  ;; Daily Note template name - requires a template directory. Default: Daily Note Template.md
                                        ;(setq obsidian-daily-note-template "Daily Note Template.md")
  :bind (:map obsidian-mode-map
              ;; Replace C-c C-o with Obsidian.el's implementation. It's ok to use another key binding.
              ("C-c C-o" . obsidian-follow-link-at-point)
              ;; Jump to backlinks
              ("C-c C-b" . obsidian-backlink-jump)
              ;; If you prefer you can use `obsidian-insert-link'
              ("C-c C-l" . obsidian-insert-wikilink)))
