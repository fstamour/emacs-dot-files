

;; TODO disable wrapping == enable truncation
(add-hook 'json-mode-hook '((lambda ()
                              (toggle-truncate-lines 1))))
