;; The one packaged in guix is called "prettier-js", which is
;; "official" but does not seem to be maintained and it looks like
;; it only support js???
;; The one from guix is https://github.com/prettier/prettier-emacs

;; TODO make the function "prettier-js" an autoload



;; https://github.com/jscheid/prettier.el

;; (use-package "prettier"
;;   :ensure t
;;   :config
;;   (add-to-list 'prettier-major-mode-parsers
;;                `(js-json-mode . ,(cdr (assoc 'json-mode prettier-major-mode-parsers)))))


;; ;; (prettier--parsers)

;; ;; prettier-editorconfig-flag - defaults to t
;; ;; prettier-enabled-parsers

;; ;; I use fsta/maybe-format-on-save instead of prettier-prettify-on-save-flag
