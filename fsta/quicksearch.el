;;;; "Quicksearch" is basically a "launch bar" for frequently used shortcuts.

(require 'cl-lib)

(defun fsta/call-from-client (fn)
  "Helper to call FN from emacsclient."
  (let ((had-focus (frame-focus-state)))
    (unless had-focus
      (raise-frame))
    (unwind-protect
        (funcall fn)
      (unless had-focus
        (suspend-frame)))))

(defun fsta/open (thing)
  ;; TODO Docstring
  "Opens stuff"
  (message "Opening \"%s\"..." thing)
  (cond
   ((string-suffix-p ".ps1" thing t)
    (call-process "powershell.exe" nil
                  0 nil
                  ;; t (get-buffer-create "*fsta/open*")
                  (format "& \"%s\"" thing)))
   (t
    (if (memq system-type '(windows-nt ms-dos))
        (call-process "explorer" nil
                      0 nil
                      ;; t (get-buffer-create "*fsta/open*")
                      thing)
      (message "NOT IMPLEMENTED YET, call xdg-open?")))))

;; For debugging, pass 't (get-buffer-create "*fsta/open*")' instead of '0 nil' to call-process


(defcustom fsta/quicklinks-directory ""
  "A directory that contains *.url files."
  :type 'directory
  :group 'fsta)

(defcustom fsta/quickfolders '()
  "A list of directory that contains project directories."
  :type '(repeat directory)
  :group 'fsta)

(defcustom fsta/quicksearch/candidates '()
  "A list of functions or list of items that constitutes the list of candidates in quicksearch."
  :type '(repeat sexp)
  :group 'fsta)

(defun fsta/list-quicklinks ()
  "List the links in the quicklinks directory"
  (mapcar
   #'(lambda (path)
       (subst-char-in-string ?/ ?\\ path))
   (directory-files fsta/quicklinks-directory
                    t ;; return absolute file names
                    (rx
                     (* anything)
                     "."
                     (or
                      "url" "lnk" "ps1")
                     eol))))

;; (fsta/list-quicklinks)


(defun fsta/quicksearch/quicklinks ()
  "Format the quicklinks like quicksearch is expecting."
  (mapcar
   #'(lambda (path)
       `(,(file-name-base path) (:url ,path)))
   (fsta/list-quicklinks)))

;; (fsta/quicksearch/quicklinks)


(defun fsta/quicksearch/quickfolders ()
  "I have folders, full of folders that I often open with vscode or emacs."
  ;; I _could_ use projectile for opening those in emacs, but it's
  ;; "hard" to get projectile to list them all
  (mapcan
   #'(lambda (path)
       `((,(format "Open folder \"%s\" in vscode."
                   (file-name-base path)) (:vscode ,path))
         (,(format "Open folder \"%s\" in emacs"
                   (file-name-base path)) (:emacs ,path))
         (,(format "Open folder \"%s\" in file manager"
                   (file-name-base path)) (:url ,path))))
   (mapcar
    #'(lambda (path)
        (subst-char-in-string ?/ ?\\ path))
    (mapcan
     #'(lambda (directory)
         (directory-files
          directory
          t ;; return absolute file names
          (rx line-start (not "."))))
     fsta/quickfolders))))

;; (fsta/quicksearch/quickfolders)

(defun fsta/quicksearch/gitlab-projects ()
  ;; TODO Docstring
  (when  (boundp '*fsta/gitlab-projects*)
    (cl-loop for project across *fsta/gitlab-projects*
             append `((,(format "%s in (%s)"
                                (assoc-default 'name project)
                                (assoc-default 'name_with_namespace project))
                       (:url ,(assoc-default 'web_url project)))
                      (,(format "New Issue in %s (%s)"
                                (assoc-default 'name project)
                                (assoc-default 'name_with_namespace project))
                       (:url ,(format "%s/-/issues/new?issuable_template=issue"
                                      (assoc-default 'web_url project))))))))

(defun fsta/quicksearch/candidates ()
  "Generate all the candidates for quicksearch."
  (mapcan
   #'(lambda (spec)
       (if (symbolp spec)
           (funcall spec)
         spec))
   fsta/quicksearch/candidates))

;; (length (fsta/quicksearch/candidates))


(defun fsta/quicksearch/generate-translate-dynamic-targets ()
  (let ((lang '(("f" "fr" "french")
                ("e" "en" "english")
                ("s" "es" "spanish")
                ("r" "ru" "russian"))))
    (cl-loop for type in (split-string "fe fs ef es se sf er fr rf re")
             for from = (cdr (assoc (cl-subseq type 0 1) lang))
             for to = (cdr (assoc (cl-subseq type 1) lang))
             collect
             (list type
                   (format "Translate from %s to %s" (cl-second from) (cl-second to))
                   (list :url (format "https://www.deepl.com/translator#%s/%s/%%s" (cl-first from) (cl-first to)))))))

;; TODO add more from https://gist.github.com/fstamour/aaf259d9ee1ef5c223d7bca7f8cec809
(defvar fsta/quicksearch/dynamic-targets
  `(("man" "Search manuals on the web." (:url "https://www.die.net/search/?q=%s"))
    ("ql" "Search quickdocs" (:url "https://quickdocs.org/-/search?q=%s" "Search with quickdocs"))
    ("alt" "Alternative to" (:url "https://alternativeto.net/browse/search?q=%s"))
    ("npm" "Search in npm" (:url "https://www.npmjs.com/search?q=%s"))
    ("npmv" "Open in npm" (:url "https://www.npmjs.com/package/%s?activeTab=versions"))
    ,@(fsta/quicksearch/generate-translate-dynamic-targets)))

(defun fsta/quicksearch/interpret-dynamic-target (choice)
  (cl-destructuring-bind (dynamic-target-type dynamic-target-arg)
      (fsta/split-string1 choice)
    (when dynamic-target-type
      (let* ((dynamic-target
              (assoc dynamic-target-type
                     fsta/quicksearch/dynamic-targets))
             (target-definition (caddr dynamic-target))
             (target-type (cl-first target-definition))
             ;; TODO The dynamic targets should contains lambdas
             ;; instead The code to extract the format string and to
             ;; create the actual target would be handled in that
             ;; function.
             (target-format-string (cl-second target-definition))
             (target
              ;; Should probably think of using url-encode
              (format target-format-string dynamic-target-arg)))
        (when (and target-type target)
          (list target-type target))))))

(fsta/comment
 (fsta/quicksearch/interpret-dynamic-target "man tmux")
 (fsta/quicksearch/interpret-dynamic-target "ql cepl")
 (fsta/quicksearch/interpret-dynamic-target "ef été"))

(defun fsta/quicksearch ()
  (let* (;; (selectrum-display-action '(display-buffer-in-side-window (side . bottom)))
         (choices (append (fsta/quicksearch/candidates)
                          (mapcar #'(lambda (target)
                                      (format "%s - %s"
                                              (car target)
                                              (cl-second target)))
                                  fsta/quicksearch/dynamic-targets)))
         (choice (completing-read "Choose "
                                  choices
                                  ;; predicate
                                  nil
                                  ;; require-match
                                  nil))
         (target
          (or (car (alist-get choice choices nil nil #'string=))
              (fsta/quicksearch/interpret-dynamic-target choice)))
         (target-type (car target)))
    (message "target: %S" target)
    (if target
        (cl-ecase target-type
          (:url (fsta/open (cl-second target)))
          (:vscode (call-process "code" nil 0 nil (cl-second target)))
          (:emacs (find-file (cl-second target))))
      (error "Don't know how to handle %S" choice))))

;; (fsta/quicksearch)

(setf fsta/quicksearch/candidates
      `(fsta/quicksearch/quicklinks
        fsta/quicksearch/quickfolders
        fsta/quicksearch/gitlab-projects))

;; (length (fsta/quicksearch/candidates)) => 709!


;; WIP add bookmarks to quicksearch
(fsta/comment
 ;; Load bookmarks if not already loaded
 (unless (or (boundp 'bookmark-alist))
   (bookmark-load bookmark-default-file))

 (mapcar #'car bookmark-alist))
