(add-hook 'after-save-hook
          'executable-make-buffer-file-executable-if-script-p)


;; executable-set-magic is an autoloaded interactive compiled Lisp
;; function in ‘executable.el’.

;; (executable-set-magic INTERPRETER &optional ARGUMENT NO-QUERY-FLAG
;;                       INSERT-FLAG)

;; Set this buffer’s interpreter to INTERPRETER with optional ARGUMENT.
;; The variables ‘executable-magicless-file-regexp’, ‘executable-prefix-env’,
;; ‘executable-insert’, ‘executable-query’ and ‘executable-chmod’ control
;; when and how magic numbers are inserted or replaced and scripts made
;; executable.




;; C-c C-c	 case statement
;; C-c C-f	 for loop
;; C-c (	 function definition
;; C-c TAB	 if statement
;; C-c C-l	 indexed loop from 1 to n
;; C-c C-o	 while getopts loop
;; C-c C-r	 repeat loop
;; C-c C-s	 select loop
;; C-c C-u	 until loop
;; C-c C-w	 while loop
;;
;; See sh-mode's help for more
