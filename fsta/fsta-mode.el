;;;; Personalized global minor-mode


;; TODO It looks like what I wanted is "ICICLES"

;; TODO use transient instead!
(defvar fsta/entry-point-actions
  ;; TODO add breeze, erc, mu4e
  '(
    ("open work.org" . (lambda () (interactive)
                         (find-file "~/syncthing/notes/work.org")))
    ("open personal.org" . (lambda () (interactive)
                             (find-file "~/syncthing/notes/personal.org")))
    ("open captured.org" . (lambda () (interactive)
                             (find-file "~/syncthing/notes/captured.org")))
    ("open a project" . project-switch-project)
    ("magit list repositories" . magit-list-repositories)
    ("open a bookmark" . bookmark-jump)
    ("Look at the TODO list" . (lambda ()
                                 (interactive) (org-agenda nil "t")))
    ("start hacking lisp" . slime)
    ("waste time on news feeds" . elfeed)
    ("open emacs' manual" . info-emacs-manual)))

(defun fsta/entry-point ()
  "Choose an action to start on something."
  (interactive)
  (let* ((choice (completing-read
                  "What do you wanna do?: "
                  (mapcar 'car fsta/entry-point-actions)))
         (command (cdr (assoc choice fsta/entry-point-actions))))
    (when command
      (call-interactively command))))

(defun fsta/previous-window ()
  (interactive)
  (other-window -1))

(defun fsta/next-window ()
  (interactive)
  (other-window))

(progn
  (makunbound 'fstamour-minor-mode-map)
  (defvar-keymap fstamour-minor-mode-map
    :doc "Keymap for fstamour-keys-minor-mode."

;;; Completion
    "M-/"     'dabbrev-expand
    ;; 'hippie-expand ; Disable hippie-expand for now,
    ;; because it breaks lisp code by not keeping the
    ;; parenthesis balanced

;;; Getting around
    "M-o" 'ace-window
    "M-z" 'project-find-file              ; this shadows "zap to char"
    "C-c r"     'revert-buffer
    "C-x C-b" 'ibuffer
    ;; replace "Goto line" by avy-goto-line
    "M-g M-g" 'avy-goto-line
    "C-c C-s" 'fsta/slime-transient
    "C-<tab>"   'fsta/next-window
    "C-S-<tab>" 'fsta/previous-window
    "C-S-<iso-leftab>" 'fsta/previous-window ; -_-

;;; F-keys
    "<f1>" 'fsta/note-taking-transient
    "<f2>" 'fsta/entry-point ;; TODO replace by a transient
    "<f5>" 'fsta/save-and-recompile
    "<f7>" 'deadgrep
    "<f8>" 'magit-status
    ;; Opens the "global todo list"
    "<f9>" 'org-todo-list ;; === (org-agenda nil "t")
    "<f12>" 'fsta/insert-current-date

;;; Org mode and note taking
    "C-c l" 'note/add-link-to-note
    "C-c L" 'org-store-link
    "C-c t" 'note/insert-tags
    "C-c C-o" 'org-open-at-point
    "C-c c" 'org-capture

;;; Super key
    "s-<tab>" 'project-switch-to-buffer

;;; Hyper key (bound to right control using xkeymap)
    "H-g" 'imenu
    "H-v" 'fsta/open-in-vscode
    "H-s" 'save-some-buffers
    "H-h" 'query-replace ;; Meh...

    ;; like C-x 1,2,3, 0
    "H-1" 'delete-other-windows
    "H-2" 'split-window-below
    "H-3" 'split-window-right
    "H-0" 'delete-window

    "H-<left>" 'previous-buffer
    "H-<right>" 'next-buffer

    ;; TODO maybe move lines (or region) with
    ;; H-<up>
    ;; H-<down>

    ;; "n" for "new note" (or just "notes")
    "H-n" 'fsta/org-roam-find)

  (define-minor-mode fstamour-minor-mode
    "A minor mode with my cusotmizations."
    :init-value t
    :lighter " fsta"
    ;; :keymap 'fstamour-minor-mode-map
    ))

(fstamour-minor-mode 1)
