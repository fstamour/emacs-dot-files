
;; When editing org-files with source-blocks, we want the source
;; blocks to be themed as they would in their native mode.
(setq org-src-fontify-natively t)

;; Make TAB behave like "as if it were issued in the language major
;; mode buffer"
(setq org-src-tab-acts-natively t)

;; Don't confirm when evaluating a src block
(setq org-confirm-babel-evaluate nil)

;; Don't confirm when clicking elisp links..
(setq org-link-elisp-confirm-function nil)

;; Don't indent the code inside begin_src blocks
(setq org-edit-src-content-indentation 0)

;; Support more languages
(org-babel-do-load-languages
 'org-babel-load-languages
 (cl-loop for lang in '(
                        R
                        emacs-lisp
                        lisp
                        python
                        plantuml
                        shell
                        )
          collect (cons lang t)))


;;; PlantUML

;; nix-env -i plantuml openjdk
(let ((plantuml (executable-find "plantuml")))
  (when plantuml
    (setq org-plantuml-jar-path
          (expand-file-name
           (concat (file-name-directory plantuml)
                   "../lib/plantuml.jar")))))
