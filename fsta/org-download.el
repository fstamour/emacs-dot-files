
;;; https://github.com/abo-abo/org-download
;;; use M-x org-download-clipboard to paste images
(use-package org-download)
