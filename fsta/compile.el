
(setf compilation-scroll-output t)


;;; Trying to control where the compilation buffer pops up

;; Change the default name to include the project name
(setf compilation-buffer-name-function
      (lambda (name-of-mode)
        (let ((project (project-current nil)))
          (if project
              (concat "*" (downcase name-of-mode) "<" (project-name project) ">" "*")
            (concat "*" (downcase name-of-mode) "*")))))

;; Default: (setf display-buffer-alist nil)

(customize-set-variable
 'display-buffer-alist
 '(("\\*compilation*"
    (display-buffer-reuse-window display-buffer-in-previous-window display-buffer-no-window)
    (allow-no-window . t)
    (inhibit-same-window . t)
    (reusable-frames . visible))))

(defun fsta/project/find-compilation-buffers ()
  (when-let ((project (project-current nil)))
    (seq-filter
     (lambda (buffer)
       (cl-search "*compilation" (buffer-name buffer)))
     (project-buffers project))))

;; (fsta/project/find-compilation-buffers)

(defun fsta/set-window-dedicated-p (buffer-name flag)
  "If the buffer exists, and is currently displayed by a window, make
the window dedicated the the buffer."
  (when-let ((buffer (get-buffer buffer-name))
             (window (get-buffer-window buffer t)))
    (set-window-dedicated-p window flag)))

(defun fsta/toggle-window-dedicated-p ()
  "Make the current buffer's window dedicated to that buffer."
  (interactive)
  (when-let ((buffer (current-buffer))
             (window (get-buffer-window buffer t)))
    (set-window-dedicated-p window (not (window-dedicated-p)))))

(defun fsta/save-and-recompile ()
  "Recompile, but re-use the right buffer."
  (interactive)
  (when buffer-file-name (save-buffer))
  (let ((project (project-current nil)))
    ;; (message "fsta/save-and-recompile project: %S" project)
    (if project
        (let ((default-directory (project-root (project-current t))))
          (with-temp-buffer
            (envrc-mode 1)
            (let ((project-compilation-buffers (fsta/project/find-compilation-buffers)))
              (if project-compilation-buffers
                  (with-current-buffer (cl-first project-compilation-buffers)
                    (envrc-mode 1)
                    (recompile))
                (project-compile)))))
      (recompile))))


;;; Kill the process if it's already running

(setf compilation-always-kill t)


;;; Trying to set the compile-command automatically

(defun fsta/setup-compile-command ()
  ;; (message "fsta/setup-compile-command: %S %S" (current-buffer) major-mode)
  ;; (case major-mode)
  ;; (setq-local compile-command "...")
  ;; (when buffer-file-name (shell-quote-argument buffer-file-name))
  )

;; Wrong hooks:
;; (add-hook 'change-major-mode-hook 'fsta/setup-compile-command)
;; (remove-hook 'change-major-mode-hook 'fsta/setup-compile-command)

(add-hook 'after-change-major-mode-hook 'fsta/setup-compile-command)



;;; Color

(require 'ansi-color)

(add-hook 'compilation-filter-hook 'ansi-color-compilation-filter)

;; (compile "grep -iR color --color=always")



;;; VARIABLES!!!

;; (setq-local compilation-environment '("ENV=local"))

;; maybe try compilation-process-setup-function to set that value interactively

;; N.B. compile-command is EVALUATED
;; compile-command compilation-arguments


;;; Error regexp

;; compilation-error-regexp-alist
;; compilation-error-regexp-alist-alist


;; (push '(grep-recursive "^\\([^:\n]+\\):" 1) compilation-error-regexp-alist-alist)

;; (push '("^\\([^:\n]+\\):\\([0-9]+\\)?:?" 1 2) compilation-error-regexp-alist)
;; (pop compilation-error-regexp-alist)

;; (compile "grep -iRn color --color=always")


;; This is cool, but there's a built-in command named "grep"


;;; Use the regexp in shell

(compilation-shell-minor-mode)



;;; Make the compilation buffer interactive (i.e. use comint mode)

;; (defadvice compile (before ad-compile-smart activate)
;;   "Advises `compile' so it sets the argument COMINT to t."
;;   (ad-set-arg 1 t))
