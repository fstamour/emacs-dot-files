;; Requires to have "w3m" in the path (which isn't true on windows)
;; Integrated web browser
(unless (eq system-type 'windows-nt)
  (use-package w3m
    :config (setq
             browse-url-browser-function 'w3m-browse-url-other-window
             w3m-display-mode 'dual-pane)))

;; from https://www.emacswiki.org/emacs/WThreeMHintsAndTips
(defun w3m-browse-url-other-window (url &optional newwin)
  "Create a new window and browse there."
  (let ((w3m-pop-up-windows t))
    (if (one-window-p) (split-window))
    (other-window 1)
    (w3m-browse-url url newwin)))
