;;; Trying out winner-mode

;; global minor mode that records the changes in the window configuration

;; C-c left (winner-undo)
;; C-c right (M-x winner-redo)

(winner-mode 1)

;; Customizations:
;; - winner-dont-bind-my-keys (boolean)
;; - winner-ring-size (default 200 window configurations per frame)

;; If there are some buffers whose windows you wouldn’t want Winner
;; mode to restore, add their names to the list variable
;; winner-boring-buffers or to the regexp
;; winner-boring-buffers-regexp.
