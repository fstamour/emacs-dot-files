;; Completion in the mini-buffer
(icomplete-mode 1)

;; The defaults:
;; (setq completion-styles '(basic partial-completion emacs22))

(setq completion-styles '(basic initials partial-completion flex))
(setq completion-cycle-threshold 10)

;; see also completion-category-defaults and
;; completion-category-overrides
