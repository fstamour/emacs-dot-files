;; https://github.com/alphapapa/org-super-agenda
;; This seems cool, but I _just_ started using agendas... so not
;; really useful yet

(use-package org-super-agenda)

(setq org-super-agenda-groups
      '(;; Each group has an implicit boolean OR operator between its selectors.
        (:name "Today"           ; Optionally specify section name
               :time-grid t      ; Items that appear on the time grid
               :todo "TODAY")))
