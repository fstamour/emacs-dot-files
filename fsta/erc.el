;; See https://www.irchelp.org/
;; See https://www.emacswiki.org/emacs/ErcStartupFiles
;; See https://systemcrafters.net/live-streams/june-04-2021/
;; erc-insert-pre-hook (see https://www.emacswiki.org/emacs/ErcHooks)
;; could be useful to hack some stuff together
;;
;; Note on cloaking: you need to join the #libera-cloak channel and
;; say "~cloakme" (that's a one-time thing).

;; TODO autoload
(require 'erc-auto)

(setq
 erc-nick "fstamour"
 erc-user-full-name "Francis St-Amour")



;;; Auto join channels

;; Load the module "erc-services" which is used to identify with
;; /NickServ automatically (a.k.a. auto-identification)
(require 'erc-services nil t)
(erc-services-mode 1)

(setq erc-autojoin-channels-alist
      '(("Libera.Chat"
         "#sbcl"
         "#guix"
         "#nonguix"
         "#clim"
         "#lisp"
         "#emacs"
         "##forth"
         "#gardening")))

;; Other maybe interesting channels:
;; - #nixos #nixos-* #nix-*
;; - #nyxt
;; - #infra-talk
;; - #llm ##machinelearning ##machinelearning-general
;; - #math
;; - ##networking
;; - ##nix
;; - #org-roam #org-mode #org-mode-fr
;; Use /list to see the available channels



;;; Auto-identify (/NickServ)

;; Don't ask for password
(setq erc-prompt-for-nickserv-password nil)
;; (setq auth-source-debug t)
;; use .netrc for password
(setq erc-use-auth-source-for-nickserv-password t)


;;; Custom Commands

(defun fsta/libera.chat ()
  "Connect to libera.chat IRC server."
  (interactive)
  (switch-to-buffer
   (erc :server "irc.libera.chat"
        :port 6667)))


;;; Logging
;;; See https://www.emacswiki.org/emacs/ErcLogging

(cl-pushnew 'log erc-modules)

(setq erc-log-channels-directory
      (expand-file-name "~/.erc/logs/"))

(mkdir erc-log-channels-directory t)

(setq erc-save-buffer-on-part nil
      erc-save-queries-on-quit nil
      erc-log-write-after-send t
      erc-log-write-after-insert t)


;; (erc-save-buffer-in-logs)



;;; Notifications

(setq erc-echo-notices-in-minibuffer-flag t)

(setq
 ;; erc-track-exclude '("#emacs")
 erc-track-exclude-types '("JOIN" "NICK" "QUIT" "MODE"
                           "AWAY" "333" "353")
 erc-hide-list '("JOIN" "NICK" "QUIT" "MODE" "AWAY")
 erc-track-exclude-server-buffer t)

;; see erc-echo-notice-hook


;;; Æsthetics

(require 'erc-match)
(setq erc-keywords '("fstamour"))

(setq erc-fill-column 120
      erc-fill-function 'erc-fill-static
      ;; Default is 27
      erc-fill-static-center 20)

(setq erc-interpret-mirc-color t)




(erc-update-modules)
