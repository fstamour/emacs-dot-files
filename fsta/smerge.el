;; This file is loaded only after smerge-mode is loaded
;; (with-eval-after-load 'smerge-mode ...)

(defun fsta-smerge-keep-upper-then-next ()
  (interactive)
  (smerge-keep-upper)
  (smerge-next)
  (recenter-top-bottom 0))

(defun fsta-smerge-keep-lower-then-next ()
  (interactive)
  (smerge-keep-lower)
  (smerge-next)
  (recenter-top-bottom 0))

(defun fsta-smerge-keep-all-then-next ()
  (interactive)
  (smerge-keep-all)
  (smerge-next)
  (recenter-top-bottom 0))

(progn
  (define-key smerge-mode-map (kbd "M-n") 'smerge-next)
  (define-key smerge-mode-map (kbd "M-p") 'smerge-prev)

  (define-key smerge-mode-map (kbd "M-a") 'fsta-smerge-keep-all-then-next)
  (define-key smerge-mode-map (kbd "M-L") 'smerge-keep-lower)
  (define-key smerge-mode-map (kbd "M-U") 'smerge-keep-upper)
  (define-key smerge-mode-map (kbd "M-l") 'fsta-smerge-keep-lower-then-next)
  (define-key smerge-mode-map (kbd "M-u") 'fsta-smerge-keep-upper-then-next))

;; Tips: use smerge-diff-base-upper when rebasing and the changes from
;; upstream are not clear.
