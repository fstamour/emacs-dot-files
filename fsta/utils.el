(require 'cl-lib)


;;; Loading files

;; Because emacs on windows has a weird home
(defun fsta/actual-home ()
  (file-name-as-directory
   (or
    (getenv "USERPROFILE")
    (getenv "HOME"))))

(defun fsta/from-home (&rest strings)
  (apply 'concat (fsta/actual-home) strings))

(defun fsta/load-from-home-if-exists (path)
  "Load a file, if it exists, where PATH is a path relative to home directory.
Return t if the file was loaded, nil otherwise"
  (let ((file (fsta/from-home path)))
    (if (file-exists-p file)
        (progn
          (load file)
          t)
      (progn
        (warn "Could not find \"%s\"." file)
        nil))))

(defun fsta/load-from-home-if-exists* (paths)
  (let ((file
         (cl-loop for path in paths
                  for file = (fsta/from-home path)
                  until (file-exists-p file)
                  finally return file)))
    (if file
        (load file)
      (warn "Could not find any of %S from home." paths))
    ;; convert to t or nil
    (and file t)))


;;; String manipulation

(defun fsta/string-contains-newline (string)
  "Check if a STRING contains a newline."
  (find-if #'(lambda (char)
               (or (eq ?\n char)
                   (eq ?\r char)))
           string))


(defun fsta/extract-columns (string)
  "Given a STRING, find the index of each columns."
  (loop for end = 0 then (match-end 0)
        for start = (string-match "\\<[^ ]" string end)
        while start
        collect start))


;; (fsta/extract-columns "a  b ee")
;; => (0 3 5)

(defun fsta/string-partition (string positions)
  "Split STRING at each POSITIONS."
  (loop for i from 0 below 10
        for positions-left on positions
        for start = (car positions-left)
        for end = (car (cdr positions-left))
        while start
        collect (subseq string start end)))

;; (fsta/string-partition "abce" '(0 1 3))
;; => ("a" "bc" "e")

(defun fsta/url-p (string)
  "Is STRING a valid url?"
  (url-fullness
   (url-generic-parse-url string)))

;; TODO rename string-count-char-if perhaps
(defun fsta/string-count-if (string predicate)
  "Count the occurences of characters that match PREDICATE in STRING."
  (cl-loop for start = 0 then end
           for end = (position-if predicate string) then (position-if
                                                          predicate string
                                                          :start (1+ start))
           while end
           sum 1 into count
           finally (return count)))


(defun fsta/string-count-char (string char)
  "Count the occurences of CHAR in STRING."
  (fsta/string-count-if string #'(lambda (char2) (eq char char2))))

;; (fsta/string-count-char "asdf///" ?/) ;; => 3
;; (fsta/string-count-char "/" ?/) ;; => 1

(defun fsta/string-count-chars (string chars)
  "Count the occurences of CHARS in STRING."
  (let ((actual-chars (if (stringp chars)
                          (string-to-list chars)
                        chars)))
    (message "%s" actual-chars)
    (fsta/string-count-if string
                          #'(lambda (char)
                              (member char actual-chars)))))

;; (fsta/string-count-chars "akljshdf" "ajk") ;; => 3

(defun fsta/char-uppercase-p (char)
  "Check if CHAR is an uppercase letter."
  (string= "Lu" (get-char-code-property char 'general-category)))

;; (mapcar #'fsta/char-uppercase-p '(?A ?Z ?a ?z ?∪ ?1 ?? ?\n))
;; => '(t t nil nil ...)

(cl-defun fsta/split-string1 (string &optional (char ?\s))
  "Split STRING in 2 at the position of CHAR.
CHAR is not included in the result.
Returns a list with the 2 parts
Returns nil if CHAR is not found.

The \"1\" in \"split-string1\" is because we split \"once\".
"
  (let ((pos (cl-position char string)))
    (when pos
      (list (cl-subseq string 0 pos)
            (cl-subseq string (1+ pos))))))

(cl-defun fsta/without-quotes (string &optional (quote "\""))
  ;; TODO use string-remove-prefix and string-remove-suffix
  (let ((start (if (string-prefix-p quote string) 1 0))
        (end (if (string-suffix-p quote string) -1 nil)))
    (substring string start end)))

(cl-defun fsta/without-quotes* (string)
  (fsta/without-quotes (fsta/without-quotes string "\"") "'"))

;; TODO tests!
(cl-every
 (lambda (x)
   (string= x "asdf"))
 (list
  (fsta/without-quotes "asdf")
  (fsta/without-quotes "\"asdf\"")
  (fsta/without-quotes "\"asdf")
  (fsta/without-quotes "asdf\"")))
(string= "as\"df" (fsta/without-quotes "as\"df"))


;;; Misc. macros

(defmacro fsta/measure-time (&rest body)
  "Measure the time it takes to evaluate BODY."
  `(let ((time (current-time))) ;; TODO Time should be gensym'd
     (prog1
         (progn ,@body)
       (message "%.06fs" (float-time (time-since time))))))


(defmacro fsta/comment (&rest body)
  "Comment BODY."
  nil)


;;; Files and buffer

(defun fsta/slurp-file (file)
  "Return FILE content."
  (with-temp-buffer
    (insert-file-contents file)
    (set-buffer-file-coding-system 'unix) ;; idk if this actually helps, I think not
    (buffer-string)))



;;; List manipulation

(defun fsta/keep-prefix (list-of-string)
  (let ((keep t))
    (remove-if-not
     #'(lambda (x)
         (when (string= x "-")
           (setf keep nil))
         keep)
     list-of-string)))

;; (fsta/keep-prefix '("a" "b" "c" "-" "d"))
;; => ("a" "b" "c")


;;; File manipulation

(defun fsta/delete-file-and-buffer ()
  "Kill the current buffer and deletes the file it is visiting."
  (interactive)
  (let ((filename (buffer-file-name)))
    (if filename
        (if (y-or-n-p (format "Do you really want to delete the file %S?"
                              filename))
            (progn
              (delete-file filename)
              (message "Deleted file %s." filename)
              (kill-buffer)))
      (message "Not a file visiting buffer!"))))

(defun fsta/relative-path ()
  "Get the path of the current file relative to the root of the
project."
  (when buffer-file-name
    (when-let ((root (or
                      (and
                       (fboundp 'project-current)
                       (project-current)
                       (project-root (project-current)))
                      (vc-root-dir))))
      (file-relative-name buffer-file-name root))))

;; TODO this should be "copy-buffer-file-name"
(defun fsta/copy-buffer-name ()
  (interactive)
  (let ((relative-path (or (fsta/relative-path)
                           buffer-file-name)))
    (kill-new relative-path)
    (message "Copied %S." relative-path)))

;; TODO this should be "copy-buffer-file-name..."
(defun fsta/copy-buffer-name-with-line-number ()
  (interactive)
  (let* ((path (fsta/relative-path))
         (path (format "%s:%s" path (line-number-at-pos))))
    (kill-new path)
    (message "Copied %S." path)))



;; TODO improve the indentation maybe?
;; TODO there's a built-in called executable-chmod...
(defun fsta/chmod+x ()
  (interactive)
  (let ((filename (buffer-file-name)))
    (if filename
        (let ((current-modes (file-modes filename)))
          ;; TODO check if the file is already executable?
          (if (y-or-n-p
               (format "Do you really want to make %S file executalbe? (current modes: %S)"
                       filename
                       current-modes))
              (progn (chmod filename
                            (file-modes-symbolic-to-number
                             "+x"
                             current-modes))
                     (message "The file %S is now executable (current modes: %S)"
                              filename
                              (file-modes filename)))))
      (message "Not a file visiting buffer!"))))

(fsta/comment ;; TODO make a macro for this:
 (let ((filename (buffer-file-name)))
   (if filename
       (progn ,body)
     (message "Not a file visiting buffer!"))))


;;; "Functional stuff"

(cl-defun fsta/maybe-funcall (fn &rest args)
  (if (fboundp fn)
      (apply fn args)
    (warn "\"%S\" is not fboundp" fn)))


;;; Undo

(defmacro fsta/with-undo (&rest body)
  "Execute BODY as one \"undo item\", undo everything if an error
occured when executing BODY."
  (let ((undo-marker (cl-gensym)))
    `(let (,undo-marker (prepare-change-group))
       (unwind-protect
           (with-undo-amalgamate
             ,@body)
         (undo-amalgamate-change-group ,undo-marker)))))


;;; Searching in buffers

(cl-defun fsta/buffer-substring-match (&optional (subexp 0))
  "Get the text matched by the last search
SUBEXP, a number, specifies which parenthesized expression in the last
  regexp."
  (let ((start (match-beginning subexp)))
    (when start
      (buffer-substring-no-properties start (match-end subexp)))))


;;; Comments

(defun fsta/change-comment (new-comment-start)
  "Change the current buffer's \"comment char\"."
  (interactive
   (list
    (read-string
     (format "New comment start: " comment-start)
     comment-start
     'fsta/change-comment-history)))
  (if (string= comment-start new-comment-start)
      (message "commend-start unchanged: provided comment-start is the same as the current one.")
    (message "Changing the comment-start from %S to %S" comment-start new-comment-start)
    (setq comment-start new-comment-start)))

(defun fsta/whitespace-or-empty-line-p ()
  "Check if the current line is empty or if it contains only
whitespace characters."
  (string-match "^[[:blank:]]*$"
                (buffer-substring-no-properties
                 (line-beginning-position)
                 (line-end-position))))

(defun fsta/toggle-comment-line ()
  "Comment out the current line."
  ;; comment-region refuses to comment the region if it's empty or
  ;; consists of just whitespaces (it signals the error "Nothing to
  ;; comment". So I use comment-dwim instead, for that case.
  ;;
  ;; TODO actually implement the "toggle" part 😅
  (if (fsta/whitespace-or-empty-line-p)
      (comment-dwim)
    (comment-region (line-beginning-position) (line-end-position))))



;; TODO bind to M-; (and bind comment-dwim to C-M-;)
