
;; Command goimports updates your Go import lines, adding missing ones and removing unreferenced ones.
;; https://pkg.go.dev/golang.org/x/tools/cmd/goimports
;; go install golang.org/x/tools/cmd/goimports@latest

;; TODO check if "goimports" is available
;; (setq gofmt-command "goimports")

(add-hook 'before-save-hook 'gofmt-before-save)

;; (add-hook 'go-mode-hook 'eglot)

;; TODO call go-remove-unused-imports on save
