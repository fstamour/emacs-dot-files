
(require 'company)
(require 'yasnippet)

;; (require 'eglot)

(defun fsta/maybe-format-on-save ()
  (cond
   ((bound-and-true-p prettier-mode) (prettier-prettify))
   ((and (boundp 'eglot-current-server)
         (eglot-current-server))
    (eglot-format-buffer))))

(with-eval-after-load 'eglot
  (add-to-list 'eglot-server-programs
               '(terraform-mode . ("terraform-ls" "serve")))
  (add-to-list 'eglot-server-programs
               '(json-mode . ("typescript-language-server" "--stdio")))
  (define-key eglot-mode-map (kbd "C-.")
              #'eglot-code-actions
              ;; #'eglot-code-action-quickfix
              )
  (custom-set-variables
   '(eglot-confirm-server-initiated-edits nil))
  (add-hook 'before-save-hook 'fsta/maybe-format-on-save))

;; (define-key eglot-mode-map (kbd "C-c <tab>") #'company-complete) ; initiate the completion manually
;; (define-key eglot-mode-map (kbd "C-c e f n") #'flymake-goto-next-error)
;; (define-key eglot-mode-map (kbd "C-c e f p") #'flymake-goto-prev-error)
;; (define-key eglot-mode-map (kbd "C-c e r") #'eglot-rename)



;; TODO (add-hook 'before-save-hook #'eglot-organize-imports 30 t)



;; TODO C-e transcient
;; r rename
;; o organize imports   eglot-code-action-organize-imports
;; f eglot-format (fsta/maybe-format-on-save)
;; F eglot-format-buffer

;; (setq-default eglot-workspace-configuration
;;               '((:gopls .
;;                         ((staticcheck . t)
;;                          (matcher . "CaseSensitive")))))


;; TODO organize imports on save
(fsta/comment
 (add-hook 'before-save-hook
           (lambda ()
             (call-interactively 'eglot-code-action-organize-imports))
           nil t))
