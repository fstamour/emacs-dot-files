
(use-package helm
    :bind (("M-x" . helm-M-x)
	   ("C-c k" . helm-show-kill-ring)
	   ("C-x C-f" . helm-find-files)
	   ("C-s" . helm-occur))
    :config
    (helm-mode 1)
    (define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action))

;; projectile: "project interaction library"
(use-package helm-projectile)
(use-package projectile
  :bind (("C-c p" . 'helm-projectile))
  :config (projectile-mode +1))

;; Make the currently active key bindings interactively searchable
;; with helm.
(use-package helm-descbinds)
