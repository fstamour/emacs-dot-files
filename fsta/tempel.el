;; https://github.com/minad/tempel

(use-package tempel
  :config
  (defun fsta/add-tempel-to-capfs ()
    (interactive)
    (make-local-variable 'completion-at-point-functions)
    (cl-pushnew #'tempel-complete
                completion-at-point-functions))
  ;; (add-hook 'conf-mode-hook 'fsta/add-tempel-to-capfs)
  ;; (add-hook 'prog-mode-hook 'fsta/add-tempel-to-capfs)
  ;; (add-hook 'text-mode-hook 'fsta/add-tempel-to-capfs)
  )

;; tips: used C-M-i to call complete-at-point

;; (add-hook 'lisp-mode #'tempel-abbrev-mode)
