;;;; Remove lighters

(use-package diminish)

(diminish 'paredit-mode "()")
(diminish 'abbrev-mode)
(diminish 'eldoc-mode)
(diminish 'emacs-configuration-org-mode)
(diminish 'auto-fill-mode)
(diminish 'prettier-mode)

