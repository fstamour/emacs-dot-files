;; tell emacs where to read abbrevs from
(setq abbrev-file-name "~/.emacs.d/abbrev_defs")

;; save abbrevs when files are saved
(setf save-abbrevs 'silently)

;; enable abbrev-mode everywhere
(setq-default abbrev-mode t)

;; fix stupid case change on dynamic abbrevs
(setq dabbrev-case-fold-search 1)
