
(cl-defun fsta/%open-in-vscode (filename &optional line-number)
  (message "fsta/%%open-in-vscode: filename = %S line-number = %S" filename line-number)
  (let* (;; (filename (if (bufferp filename) (buffer-file-name filename) filename))
         (args (if line-number
                   (list "--goto"
                         (concat filename
                                 ;; see implementation of (what-line) to know how to handle narrowed buffers
                                 ":" (number-to-string (1+ line-number))))
                 (list filename))))
    (apply 'call-process
           "code"
           nil ;; use /dev/null as input
           0   ;; don't wait for the process to terminate
           nil ;; don't redisplay anything
           ;; The arguments to pass to code:
           args)))


;; (when (region-active-p) (thing-at-point 'existing-filename t))

(defun fsta/open-in-vscode ()
  (interactive)
  (let* ((filename (buffer-file-name))
         (filename-at-point (unless filename
                              (thing-at-point 'existing-filename t)))
         (magit-current-file (unless filename-at-point
                               (magit-current-file))))
    (cond
     (filename
      (save-buffer)
      (fsta/%open-in-vscode filename (line-number-at-pos)))
     (filename-at-point
      (save-buffer)
      (fsta/%open-in-vscode filename))
     (magit-current-file
      ;; TODO this only works in magit's diff or status buffer (AFAIK)
      (cl-destructuring-bind (buffer pos)
          (magit-diff-visit-file--noselect nil t)
        (let ((line-number (with-current-buffer buffer
                             (line-number-at-pos pos))))
          (fsta/%open-in-vscode magit-current-file line-number))))
     (t (error "Don't know how to open this in vscode...")))))
