
;; https://github.com/purcell/envrc
(use-package envrc
  :ensure t
  :config (envrc-global-mode))


(use-package inheritenv
  :ensure t
  :after envrc)
