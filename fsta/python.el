;; Autocompletion for python
;; https://stackoverflow.com/questions/25669809/how-do-you-run-python-code-using-emacs
;; pip install virtualenv
;; M-x jedi:install-server
;; homepage: http://tkf.github.io/emacs-jedi/released/
;; see https://www.emacswiki.org/emacs/PythonProgrammingInEmacs
(use-package jedi
  :config (progn
            (add-hook 'python-mode-hook 'jedi:setup)
            (setq jedi:complete-on-dot t)))


;; Reload python modules
;; https://stackoverflow.com/questions/684171/how-to-re-import-an-updated-package-while-in-python-interpreter


(setf python-environment-directory "~/.virtualenvs/")

(use-package virtualenvwrapper
  :config (progn
            ;; Not sure this is actually needed...
            (fsta/comment
             (add-hook 'venv-postactivate-hook
                       #'(lambda ()
                           (setq jedi:environment-root venv-current-dir))))))
