;; (use-package sly)

;; Switch to slime
(fsta/comment
 (progn
   (remove-hook 'lisp-mode-hook 'sly-editing-mode)
   (setf *fsta/sly-or-not-slime* nil)
   (unload-feature 'sly t)))


;;; Load and configure sly

(when *fsta/sly-or-not-slime*
  (let ((sly-dir (fsta/from-home "quicklisp/local-projects/sly")))
    (when (file-directory-p sly-dir)
      (add-to-list 'load-path sly-dir)
      (require 'sly-autoloads))))
