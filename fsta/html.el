
;; https://github.com/smihica/emmet-mode
(use-package emmet-mode
  :config
  (progn
    (add-hook 'html-mode-hook #'emmet-mode)
    (add-hook 'mhtml-mode-hook #'emmet-mode)
    (add-hook 'sgml-mode-hook #'emmet-mode)
    (add-hook 'css-mode-hook #'emmet-mode)))

(define-skeleton html5-boilerplate
  ""                                ; no prompt
  "<!DOCTYPE html>" \n
  "<html>" \n
  > "<head>" \n
  > "<title>" (skeleton-read "Title: ") "</title>" \n
  > "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />" \n
  > "<!-- Don't fetch favicon -->" \n
  > "<link rel=\"icon\" href=\"data:,\" />" \n
  > "</head>" \n
  > "<body>" \n
  > | \n
  > "</body>" \n
  > "</html>")
