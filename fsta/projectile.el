
;; From projectile...
(defvar fsta/defaults/projectile-other-file-alist
  '(("cpp" "h" "hpp" "ipp")
    ("ipp" "h" "hpp" "cpp")
    ("hpp" "h" "ipp" "cpp" "cc")
    ("cxx" "h" "hxx" "ixx")
    ("ixx" "h" "hxx" "cxx")
    ("hxx" "h" "ixx" "cxx")
    ("c" "h")
    ("m" "h")
    ("mm" "h")
    ("h" "c" "cc" "cpp" "ipp" "hpp" "cxx" "ixx" "hxx" "m" "mm")
    ("cc" "h" "hh" "hpp")
    ("hh" "cc")
    ("ml" "mli")
    ("mli" "ml" "mll" "mly")
    ("mll" "mli")
    ("mly" "mli")
    ("eliomi" "eliom")
    ("eliom" "eliomi")
    ("vert" "frag")
    ("frag" "vert")
    (nil "lock" "gpg")
    ("lock" "")
    ("gpg" "")))


(defun fsta/alist-all (list)
  (cl-loop
   with list = list
   for el in list
   collect `(,el ,@ (cl-remove el list))))

;; (fsta/alist-all '(1 2 3))
;; =>((1 2 3) (2 1 3) (3 1 2))

(defun fsta/projectile-nest-file-extensions ()
  (append
   '(;; This one works only when projectile-find-other-file is called with a prefix
     "-v2.ts"
     "module.ts")
   (cl-loop
    for kind in '("service" "controller" "repository")
    append (cl-loop
            for variant in '("" ".spec" ".integration.spec")
            collect (concat kind variant ".ts")))))

;; (fsta/projectile-nest-file-extensions)
;;   =>
;; ("service.ts" "service.spec.ts" "service.integration.spec.ts"
;; "controller.ts" "controller.spec.ts" etc...

(use-package projectile
  :ensure t
  :diminish projectile-mode
  :init (projectile-mode 1)
  :bind (:map projectile-mode-map
              ("C-c p" . projectile-command-map))
  :config (progn
            ;; default is 'alien, but that method ignores .projectile file
            (setf projectile-indexing-method 'hybrid)
            (cl-pushnew "archive" projectile-globally-ignored-directories :test 'string=)
            (setf projectile-other-file-alist
                  (append
                   (fsta/alist-all (fsta/projectile-nest-file-extensions))
                   fsta/defaults/projectile-other-file-alist))))


;; TODO (setf projectile-project-search-path '())

;; The variable projectile-project-root-functions controls how the
;; root of the projects are found.
;;
;; Use M-x projectile-add-known-project to manually add a project
;; Or eval this: (call-interactively 'projectile-add-known-project)
;; Or that: (projectile-add-known-project "path-to-the-project")
