;; TODO Rename this file to "version-control"

(defun fsta/extract-first-integer-from-string (string)
  (and (string-match "[[:digit:]]+" string)
       (match-string 0 string)))

(defun fsta/magit-extract-tracker-id-from-branch ()
  (and (magit-get-current-branch)
       (fsta/extract-first-integer-from-string
        (magit-get-current-branch))))

;; TODO does this work?
(defun fsta/git-rebase-p ()
  (file-exists-p
   (concat (magit--safe-default-directory) "REBASE_HEAD")))

(defun fsta/changelog-in-staged-files-p ()
  (let ((case-fold-search t))
    (some (lambda (file)
            (and
             (string-match (rx-to-string '(: bos "changelog")) file)
             t))
          (magit-staged-files))))

;; FIXME > Hook run after entering or leaving ‘git-commit-mode’.
;; We surely don't want to run this when _leaving_ the mode
(fsta/comment
 (defun fsta/magit-commit-message-template (&rest discard)
   (if (magit-get-current-branch)
       ;; Damn... I forgot what the current-prefix-arg is supposed to mean in this case..g.
       (progn
         (message "current-prefix-arg: %S" current-prefix-arg)
         (unless current-prefix-arg
           (when-let ((tracker-id (fsta/magit-extract-tracker-id-from-branch))
                      (prefix (format "#%s " tracker-id)))
             (fsta/comment
              (message "===================== fsta/changelog-in-staged-files-p:")
              (message "%S" (fsta/changelog-in-staged-files-p))
              (when (fsta/changelog-in-staged-files-p)
                (setf prefix (concat prefix "update changelog"))))
             ;; Don't prepend the prefix if it's already there
             (unless (string= (buffer-substring 1 (1+ (length prefix))) prefix)
               (insert prefix)))))
     ;; TODO Unless we're amendig a commit
     (unless (fsta/git-rebase-p)
       (insert "Hey! you're committing to a detached HEAD...")))))

(defun fsta/magit-commit-message-template (&rest discard)
  )



(use-package magit
  ;; :defer 1
  :config

  ;; Add my hook, but keep 'git-commit-save-message first, assuming it's
  ;; already the first in the list of hooks.
  (unless (member 'fsta/magit-commit-message-template git-commit-setup-hook)
    (setf git-commit-setup-hook
          (append
           '(git-commit-save-message fsta/magit-commit-message-template)
           (cl-rest git-commit-setup-hook))))

  ;; git-commit-setup-hook
  ;; (git-commit-save-message git-commit-setup-changelog-support git-commit-turn-on-auto-fill git-commit-propertize-diff bug-reference-mode with-editor-usage-message)

  ;; git-commit-post-finish-hook

  ;; (add-hook 'git-commit-setup-hook 'fsta/magit-commit-message-template)
  ;; (remove-hook 'git-commit-setup-hook  'fsta/magit-commit-message-template)


  (setq vc-follow-symlinks nil)


  ;; TODO Explore magit-repository-directories

  (setf magit-repository-directories
        '(("~/.config/stumpwm/" . 0)
          ("~/.config/espanso/" . 0)
          ("~/.emacs.d/" . 0)
          ("~/dev" . 2)))

  ;; TODO Better bindings in smerge
  ;; Current prefix is C-c ^, which is aweful
  ;; M-n|p instead of C-c ^ n|p

  ;; TODO Make sure the user.email is not nil
  (fsta/comment
   (cl-loop for repo in (magit-list-repos)
            collect
            (let ((default-directory repo))
              (magit-get "user.email"))))



;;; TODO Enable bug-reference-mode in magit-log-mode


  ;; TODO Try out "transient commands"
  ;; https://magit.vc/manual/transient/
  ;; https://github.com/positron-solutions/transient-showcase


  ;; https://magit.vc/manual/forge/index.html#SEC_Contents
  ;; (use-package forge
  ;;   :after magit)

  (setq auth-sources '("~/.netrc"))


  
;;; Customizing the columns in magit-list-repositories


  ;; The default value:
  (fsta/comment
   (setf magit-repolist-columns
         `(("Name" 25 magit-repolist-column-ident nil) ("Version" 25 magit-repolist-column-version ((:sort magit-repolist-version<))) ("B<U" 3 magit-repolist-column-unpulled-from-upstream ((:right-align t) (:sort <))) ("B>U" 3 magit-repolist-column-unpushed-to-upstream ((:right-align t) (:sort <))) ("Path" 99 magit-repolist-column-path nil))))

  (setf
   magit-repolist-columns
   `(("Name" 25 magit-repolist-column-ident nil)
     ;; ("Version" 25 magit-repolist-column-version ((:sort magit-repolist-version<)))
     ("F" 1 magit-repolist-column-flag nil)
     ("Unpulled" 3 magit-repolist-column-unpulled-from-upstream ((:right-align t) (:sort <)))
     ("Unpushed" 3 magit-repolist-column-unpushed-to-upstream ((:right-align t) (:sort <)))
     ("Path" 99 magit-repolist-column-path nil)))


  ;; TODO show more "recent commits"
  ;; Call M-x magit-describe-section, it'll open a *help* buffer
  (setf (alist-get 'unpushed magit-section-initial-visibility-alist) 'show)
  (setf magit-log-section-commit-count 20))
