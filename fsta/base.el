
(require 'cl-lib)
(require 'subr-x)

;; Always defaults to UTF-8 and unix end-of-lines
(setq-default buffer-file-coding-system 'utf-8-unix)

;; Don't use tabs
(setq-default indent-tabs-mode nil)

;; Delete trailing whitespaces when saving.
(add-hook 'before-save-hook #'delete-trailing-whitespace)

;;; Do the rigth thing when pressing tab
;; from https://stackoverflow.com/a/7023515/1564493
(setq tab-always-indent 'complete) ;; default is t
;; (add-to-list 'completion-styles 'initials t)

;; Flash the frame instead of playing a sound
(setq visible-bell t)

;; Use C-x C-z to suspend-frame instead
(global-set-key (kbd "C-z") 'undo)

;; To prevent polluting the git repo
(setq custom-file
      (expand-file-name "~/.emacs.d/custom.el"))
(when (file-exists-p custom-file)
  (load custom-file))

;; Only confirm before killing emacs if there are any opened files
(defun fsta/confirm-kill-emacs (prompt)
  (if (cl-remove-if-not #'buffer-file-name (buffer-list))
      (y-or-n-p prompt)
    t))

;; Confirm before exiting emacs
(setq confirm-kill-emacs 'fsta/confirm-kill-emacs)

;; Reload file from disk when they change
(global-auto-revert-mode 1)

;; Put the auto-save files in "~/.emacs.d/auto-save"
(setq auto-save-file-name-transforms
      `((".*" ,(locate-user-emacs-file "auto-save/") t)))

;; Save the temporary files in "~/.emacs.d/backups"
(setq backup-directory-alist
      `((".*" . ,(expand-file-name (concat user-emacs-directory "backups/")))))

;; Don't create .# lockfiles (it messes with other tools (that are stupid))
(setq create-lockfiles nil)

;; See also tramp-backup-directory-alist and make-backup-file-name-function

;;; Enable functions that are disabled by default
(put 'narrow-to-page 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'scroll-left 'disabled nil)
(put 'narrow-to-region 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'erase-buffer 'disabled nil)

;; Show more relevant commands in M-x
(setq read-extended-command-predicate
      #'command-completion-default-include-p)

;; Create a group of custom variables for my own functions and commands.
(defgroup fsta nil "fsta")
