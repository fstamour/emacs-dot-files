;; See
;; https://github.com/portacle/emacsd/blob/master/portacle-slime.el
;; for some more inspiration


;;; Install packages that are used only for lisp.

(use-package paredit
  :diminish paredit-mode)

(use-package rainbow-delimiters)

(defun fsta/clone-sexp ()
  (interactive)
  (save-mark-and-excursion
    (mark-sexp)
    (insert (buffer-substring-no-properties
             (region-beginning)
             (region-end))
            (if (= (line-number-at-pos (region-beginning))
                   (line-number-at-pos (region-end)))
                " "
              "\n")))
  (forward-sexp)
  (forward-char))

(define-key lisp-mode-map (kbd "C-M-y") 'fsta/clone-sexp)
(define-key emacs-lisp-mode-map (kbd "C-M-y") 'fsta/clone-sexp)

(defun fsta/enable-lisp-mode ()
  (interactive)
  (paredit-mode 1)
  (unless (memq major-mode '(slime-repl-mode lisp-mode)) ; this is a bit dumb...
    (fsta/maybe-funcall 'aggressive-indent-mode 1))
  (rainbow-delimiters-mode 1))

(add-hook 'emacs-lisp-mode-hook       #'fsta/enable-lisp-mode)
;; Not adding hooks to the minibuffer, too error prone and hard to debug
;; (add-hook 'eval-expression-minibuffer-setup-hook #'fsta/enable-lisp-mode)
;; (remove-hook 'eval-expression-minibuffer-setup-hook #'fsta/enable-lisp-mode)
(add-hook 'lisp-mode-hook             #'fsta/enable-lisp-mode)
(add-hook 'lisp-interaction-mode-hook #'fsta/enable-lisp-mode)
(add-hook 'scheme-mode-hook           #'fsta/enable-lisp-mode)
(add-hook 'slime-repl-mode-hook       #'fsta/enable-lisp-mode)

;; N.B. electric-indent-mode is a global minor mode.
(when (boundp 'aggressive-indent-dont-electric-modes)
  (cl-pushnew 'lisp-mode aggressive-indent-dont-electric-modes))

(defun fsta/maybe-disable-modes ()
  (electric-indent-mode -1)
  (when (eq major-mode 'lisp-mode)
    (electric-indent-local-mode -1)))

(add-hook 'after-change-major-mode-hook
          'fsta/maybe-disable-modes)


;;; Lisp implementation

(defvar fsta/lisp-implementations
  '((sbcl ("sbcl"
           "--noinform"
           "--dynamic-space-size" "16000")
          :coding-system utf-8-unix)))

(setq sly-lisp-implementations fsta/lisp-implementations
      sly-default-lisp 'sbcl
      slime-lisp-implementations fsta/lisp-implementations
      slime-default-lisp 'sbcl)



(defvar *fsta/sly-or-not-slime*
  (or
   (string= system-name "nu")
   ;; (string= system-name "phi")
   ))


;;; Quicklisp

(defvar fsta/quicklisp-root (expand-file-name "~/quicklisp/"))

(defun fsta/quicklisp/system-dir (distribution system &optional warn)
  "Find the directory of SYSTEM in quicklisp DISTRIBUTION."
  (let ((location-file (file-name-concat fsta/quicklisp-root
                                         "dists/"
                                         distribution
                                         "installed/systems/"
                                         (concat system ".txt"))))
    (if (file-exists-p location-file)
        (file-name-concat
         fsta/quicklisp-root
         (file-name-directory
          (string-trim-right (with-temp-buffer
                               (insert-file-contents location-file)
                               (buffer-string)))))
      (when warn
        (warn "System %S not found in distribution %S (%S), please run \"(ql:quickload 'system)\"."
              system distribution location-file)))))

;; auto load slime from quicklisp
(let ((d (fsta/quicklisp/system-dir "quicklisp" "swank" t)))
  (and d (file-directory-p d)
       (add-to-list 'load-path d)
       (require 'slime-autoloads)))

;; auto load sly from quicklisp
(let ((d (expand-file-name "../"
                           (fsta/quicklisp/system-dir "quicklisp" "slynk" t))))
  (and d (file-directory-p d)
       (add-to-list 'load-path d)
       (require 'sly-autoloads)))


;;; Configure slime

(defun fsta/slime ()
  "Just like `slime', but don't change the window configuration."
  (interactive)
  (let ((wnd (current-window-configuration)))
    (call-interactively 'slime)
    (sit-for 0.2)
    (set-window-configuration wnd)))

(defun fsta/slime-connect ()
  "Just like `slime-connect', but don't change the window configuration."
  (interactive)
  (let ((wnd (current-window-configuration)))
    (call-interactively 'slime-connect)
    (sit-for 0.2)
    (set-window-configuration wnd)))

(with-eval-after-load "slime"
  ;; Monkey-patching slime to fix an issue where a window is created every time
  ;; I used the completion.
  (advice-add 'slime-fuzzy-window-configuration-change :override
              (defun fsta/slime-fuzzy-window-configuration-change ()
                "Called on window-configuration-change-hook.  Since the window
  configuration was changed, we nullify our saved configuration."
                'NOPE)
              '((name . "fsta/slime-fuzzy-window-configuration-change")))

  ;; TODO Monkey-patching slime again to prevent to switch to the
  (fsta/comment
   (advice-add 'slime
               (defun fsta/slime-advice (original-function &rest args)
                 TODO)
               '((name . "repl-"))))
  (setq
   ;; Autocomplete
   slime-complete-symbol-function 'slime-fuzzy-complete-symbol
   ;; slime-pop-find-definition-stack
   ;; slime-edit-definition-hooks
   )
  ;; TODO mayyyybe (slime-setup '(slime-fancy))
  )


;;; Custom bindings for sly or slime

;; TODO Add bindings to go back and forth (e.g. in the inspector), see mouse.el


;;; Offline CLHS

(when *enable-offline-clhs*
  ;; Don't symlink on windows
  (defvar quicklisp-clhs-inhibit-symlink-p (eq system-type 'windows-nt)
    "Don't symlink on windows, this variable must be dynamically bound.")
  (fsta/load-from-home-if-exists "quicklisp/clhs-use-local.el"))


;;; Breeze

(defun fsta/maybe-load-and-init-breeze ()
  (interactive)
  (when (fsta/load-from-home-if-exists*
         '("dev/breeze/src/breeze.el"
           "quicklisp/local-projects/breeze/src/breeze.el"
           "nu/quicklisp/local-projects/breeze/src/breeze.el"))
    ;; Ensure breeze is loaded when a sly or slime connection is opened
    (breeze-enable-connected-hook)
    ;; Enable breeze-minor-mode in lisp-mode
    (breeze-enable-minor-mode-hook)
    ;; Enable flymake-mode in breeze-minor-mode
    (breeze-minor-mode-enable-flymake-mode)))

(with-eval-after-load 'slime
  (fsta/maybe-load-and-init-breeze))

(with-eval-after-load 'sly
  (fsta/maybe-load-and-init-breeze))

;; (progn
;;   (breeze-disable-connected-hook)
;;   (breeze-disable-minor-mode-hook)
;;   (breeze-disable-flymake-backend))

(fsta/comment
 (progn
   (breeze-disable-connected-hook)
   (breeze-disable-flymake-backend)
   (breeze-disable-minor-mode-hook)))


;;; Transient(s) for slime

(fsta/comment
 (with-eval-after-load 'slime
   (with-eval-after-load 'transient
     (defmacro fsta/slime-map-selector-methods (&rest body)
       `(cl-loop
         for (key line fn) in slime-selector-methods
         for c = (format "%c" key)
         unless (position (aref c 0) "?4npq" :test 'char-equal)
         collect (progn ,@body)))

     (cl-macrolet
         ((fuuuuun ()
            `(progn
               ,@(fsta/slime-map-selector-methods
                  `(defun ,(intern (format "fsta/slime-selector-%s" c)) ()
                     ,line
                     (interactive)
                     (let ((method (cl-find ,key slime-selector-methods :key #'car)))
                       (if method
                           (funcall (cl-third method))
                         (message "No method for character: ?\\%c" ,key)))))
               ,@(fsta/slime-map-selector-methods
                  `(defun ,(intern (format "fsta/slime-selector-%s-other" c)) ()
                     ,line
                     (interactive)
                     (let ((method (cl-find ,key slime-selector-methods :key #'car)))
                       (if method
                           (let ((slime-selector-other-window t))
                             (funcall (cl-third method)))
                         (message "No method for character: ?\\%c" ,key))))))))
       (fuuuuun))

     (fsta/slime-map-selector-methods
      (list (format "ss%c" key) line (intern (format "fsta/slime-selector-%s" c))))

     ;; (fsta/slime-selector-c)

     ;; TODO Use non-fucky way to define this (e.g. just hard-code the damn options)
     (eval
      `(transient-define-prefix fsta/slime-transient ()
         "A transient to help with slime."
         :info-manual "A transient for slime."
         ,(vector
           "Slime"
           [ ;;("ss" "Selector" slime-selector)
            ;; ("so" "Selector" slime-selector)
            ;; ("so" "Selector" slime-selector-other-window)
            ("t" "List threads" slime-list-threads)
            ("fm" "Undefine function" slime-undefine-function)
            ("C-l" "Clear REPL's buffer" slime-repl-clear-buffer)]
           (apply 'vector
                  "Selector"
                  (fsta/slime-map-selector-methods
                   (list (format "s%c" key) line (intern (format "fsta/slime-selector-%s" c)))))
           (apply 'vector
                  "Selector Other Window"
                  (fsta/slime-map-selector-methods
                   (list (format "so%c" key) line (intern (format "fsta/slime-selector-%s-other" c)))))
           ["Connection"
            ("S" "Start slime" slime)
            ("C" "Connect to swank" slime-connect)
            ("D" "Disonnect" slime-disconnect)
            ("cn" "Cycle to the next Lisp connection." slime-next-connection)
            ("cp" "Cycle to the previous Lisp connection." slime-prev-connection)
            ("RR" "Restart the inferior lisp." slime-restart-inferior-lisp)])
         ["Essential commands"
          ("q" "Cancel" transient-quit-one)]))

     (define-key slime-mode-map (kbd "C-c C-s") 'fsta/slime-transient)
     )))
