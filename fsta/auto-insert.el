;; This is WIP

;; https://www.emacswiki.org/emacs/AutoInsertMode
;; https://www.gnu.org/software/emacs/manual/html_mono/autotype.html

(auto-insert-mode 1)
;; or... (add-hook 'find-file-hook 'auto-insert)


(with-eval-after-load 'autoinsert
  (define-auto-insert
    '(html-mode . "basic html skeleton")
    '))
