

;; cmake-font-lock       Advanced, type aware, highlight support for CMake
;; cmake-ide                      Calls CMake to find out include paths and other compiler flags
;; cmake-mode                     major-mode for editing CMake sources
;; cmake-project                  Integrates CMake build process with Emacs

(use-package cmake-mode)
