(require 'server)

(defun fsta/start-server ()
  ;; Start emacs's server :D
  (unless (eq t (server-running-p))
    (server-start)))

(add-hook 'after-init-hook 'fsta/start-server)
