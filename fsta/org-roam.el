;; https://www.orgroam.com/manual.html
;;
;; the "org-roam buffers" shows the buffer's node, the backlinks, the
;; references, the unlinked references, etc.


;;; Define "org-roam projects"

(defvar fsta/org-roam-projects
  `(("myelin" . "dev/myelin/docs/")

    ("breeze" . "quicklisp/local-projects/breeze/docs/")
    ("breeze" . "dev/breeze/docs/")

    ("public-notes" . "dev/public-notes/")
    ("notes" . "notes/"))
  "Source of truth for the list of \"org-roam projects\".")

;; I use sshfs to open projects on remote host
(defvar fsta/org-roam-hosts
  (remove system-name '("nu" "phi" "s3d-fstamour"))
  "List of host to look for. The \"~\" of that host might be mounted
at \"~/<hostname>\".")

(defun fsta/org-roam-project-path-hint (relpath)
  "Return a hint about where the project is on its relative path only.

For example, it returns \"~\" for the projects located on the
host's home directory.

Used by `fsta/org-roam-project-path-hint'."
  (let* ((parts (fsta/split-string1 relpath ?/))
         (prefix (cl-first parts))
         (suffix (cl-second parts)))
    (if (and suffix (not (string-empty-p suffix)))
        (alist-get prefix '(("dev" . "dev")
                            ("quicklisp" . "ql"))
                   nil nil 'string=)
      "~")))

;; Test `fsta/org-roam-project-path-hint'
(fsta/comment
 (mapcar 'fsta/org-roam-project-path-hint
         (mapcar #'cdr fsta/org-roam-projects)))

(defun fsta/org-roam-project-hint (relpath host)
  "Return a hint about where the project is based on its relative path and the host.

TODO For example

Uses `fsta/org-roam-project-path-hint'
Used by `fsta/org-roam-projects'"
  (let ((path-hint (fsta/org-roam-project-path-hint relpath)))
    (cond
     ((and path-hint host)
      (format " (%s@%s)" path-hint host))
     ((and path-hint (not host))
      (format " (%s)" path-hint))
     ((and (not path-hint) host)
      (format " (@%s)" host))
     ((and (not path-hint) (not host))
      ""))))

;; test `fsta/org-roam-project-hint'
(fsta/comment
 (cl-loop for spec in (append `(("foo" . "sadf/ew")) fsta/org-roam-projects)
          append
          (cl-loop with (name . relpath) = spec
                   for host in (cons nil fsta/org-roam-hosts)
                   collect (fsta/org-roam-project-hint relpath host))))



(defun fsta/org-roam-projects ()
  "Actually compute the list of project (this is used with `completing-read').
This more or less does a cartesian product of the project-alist
and the list of host, then filter out the paths that don't exist."
  (cl-loop for spec in fsta/org-roam-projects
           append
           (cl-loop with (name . relpath) = spec
                    for host in (cons nil fsta/org-roam-hosts)
                    for abspath = (expand-file-name (concat host (if host "/" "./") relpath) "~")
                    when (file-directory-p abspath)
                    collect (cons (format "%s%s)"
                                          name
                                          (fsta/org-roam-project-hint relpath host))
                                  abspath))))

;; (fsta/org-roam-projects)

;; TODO I should probably just use a defvar for this, because building
;;   the list of project interacts with the file system, which may
;;   involve ssh (because of sshfs), which may introduce a lot of
;;   latency for no reason.
;;
;; TODO In the end, I didn't use it. I just straight up show
;;   `org-roam-directory' instead.
;;
;; It was meant to be used by `fsta/note-taking-transient/org-roam/description'
(defun fsta/note-taking-transient/current-project ()
  (cl-rassoc org-roam-directory
             (fsta/org-roam-projects)
             :test 'string=))

;; Make sure that `fsta/org-roam-projects' doesn't
;; return any path that don't actually exists.
(fsta/comment
 (cl-loop for (name . path) in (fsta/org-roam-projects)
          unless (file-directory-p path)
          collect name))


;;; Load org-roam

(use-package org-roam
  :ensure t
  :config
  (setq org-roam-directory (file-truename "~/notes/")
        org-roam-file-exclude-regexp '("data/" "\\.stversions/" "\\.stfolder/"))
  ;; we setup org-roam to run functions on file changes to maintain
  ;; cache consistency
  (org-roam-db-autosync-mode)
  (setq org-roam-dailies-directory "daily/")
  (setq org-roam-capture-templates
        '(("d" "default" plain "%?"
           :target (file+head "${slug}.org" "#+title: ${title}\n")
           :unnarrowed t))))
;; To tell emacs to follow symlinks?
;; (setq find-file-visit-truename t)


;;; Custom transient for common (but not common enough to remember)
;;; org-roam and org-mode commands

(with-eval-after-load 'magit
  (require 'org-roam)

  ;; I currently have F1 bound to org-roam-capture, but I would like to
  ;; use F1 to open or insert too; also I have multiple org-mode
  ;; directory (personal one in ~/notes, one for breeze and one for
  ;; public-notes).
  ;;
  ;; So, I want to create a "transient UI" to help me with this.
  ;;
  ;; TODO for now, we'll assume org-roam-db-location is
  ;; (file-name-concat org-roam-directory "org-roam.db"), I want to
  ;; extend the "projects alist" for this. Currently the "value" is just
  ;; a path, but it could be an alist too.


  (transient-define-infix fsta/transient-option/org-roam-project ()
    :description "Optionally choose a different org-roam root/project."
    :class 'transient-option
    :shortarg "-p"
    :argument "--project="
    :choices
    ;; This is magic to make sure transient's choices are in sync with
    ;; the variable fsta/org-roam-projects
    (completion-table-dynamic
     (lambda (needle)
       (fsta/org-roam-projects))))

  (defun fsta/transient-args-alist (args)
    (cl-loop for (arg) in args
             for (key value) = (when arg (split-string arg "="))
             when key
             collect (cons (intern (subseq key 2)) value)))

  ;; a variant that directly takes org-roam-directory
  ;; org-roam-db-location or project could be nice perhaps I could use
  ;; keyword arguments for this...
  (cl-defun fsta/call-with-org-roam-project (fn &key fn-args transient-args project)
    (let* ((args-alist (when transient-args (fsta/transient-args-alist transient-args)))
           (project (or project (alist-get 'project args-alist))))
      (message "project: %S" project)
      (if project
          (let* ((org-roam-directory (alist-get project fsta/org-roam-projects
                                                nil nil 'string=))
                 (org-roam-db-location
                  (file-name-concat org-roam-directory "org-roam.db")))
            (apply fn fn-args))
        (apply fn fn-args))))

  (defun fsta/org-roam-capture (&rest args)
    (interactive (list (transient-args 'fsta/note-taking-transient)))
    (fsta/call-with-org-roam-project 'org-roam-capture :transient-args args))

  (defun fsta/org-roam-node-find (&rest args)
    (interactive (list (transient-args 'fsta/note-taking-transient)))
    (fsta/call-with-org-roam-project 'org-roam-node-find :transient-args args))

  (defun fsta/org-roam-node-insert (&rest args)
    (interactive (list (transient-args 'fsta/note-taking-transient)))
    (fsta/call-with-org-roam-project 'org-roam-node-insert :transient-args args))

  (defun fsta/org-roam-switch ()
    "Interactively choose a project and switch to it.

The list of project is defined by the variables
`fsta/org-roam-projects' and
`fsta/org-roam-hosts' and is computed by the
function `fsta/org-roam-projects'."
    (interactive)
    (let ((projects-alist (fsta/org-roam-projects)))
      (when-let ((project (completing-read "Project: "
                                           ;; collection
                                           projects-alist
                                           ;; predicate
                                           nil
                                           ;; require-match
                                           t))
                 (directory (alist-get project projects-alist
                                       nil nil 'string=)))
        (setf org-roam-directory directory
              org-roam-db-location
              (file-name-concat org-roam-directory
                                (concat "org-roam." (system-name) ".db"))))))

  (defun fsta/roam/open-directory ()
    "Open the current `org-roam-directory' in emacs."
    (interactive)
    (find-file org-roam-directory))

  ;; Test:
  ;; (fsta/org-roam-switch)

  ;; TODO org-open-at-point???
  ;; TODO
  ;; TODO maybe org-roam-buffer-display-dedicated

  (defun fsta/note-taking-transient/org-roam/description ()
    "Dynamic description for the \"Org-roam\" group in the transient
`fsta/note-taking-transient'."
    (format "Org-roam - %s"
            ;; (car (fsta/note-taking-transient/current-project))
            org-roam-directory))

  (defun fsta/note-taking-transient/breeze-capture ()
    (interactive)
    ;; this will error if sly or slime is not connected.
    (breeze-listener-connected-p)
    (breeze-capture))

  (transient-define-prefix fsta/note-taking-transient (current-project)
    "A transient thingy to help with org-roam and org-mode."
    :info-manual "A transient thingy to help with org-roam and org-mode."
    ["Org-roam"
     :description fsta/note-taking-transient/org-roam/description ;;(format "hi %s" 32)
     [("c" "Capture" fsta/org-roam-capture)
      ("f" "Find" fsta/org-roam-node-find)
      ("i" "Insert" fsta/org-roam-node-insert)]
     ["Edit"
      ("at" "Add tag" org-roam-tag-add)
      ("rt" "Remove tag" org-roam-tag-remove)
      ("ar" "Add a REF" org-roam-ref-add)
      ("rr" "Remove a REF" org-roam-ref-remove)
      ("aa" "Add alias" org-roam-alias-add)
      ("ra" "Remove alias" org-roam-alias-add)]
     ["Dailies"
      ;; ("dd" "Capture today" org-roam-dailies-capture-today)
      ("dd" "Today" org-roam-dailies-goto-today)
      ("dy" "Yesteday" org-roam-dailies-capture-yesterday)
      ;; ("dgt" "Go to today" org-roam-dailies-goto-today)
      ;; ("dgy" "Go to yesteday" org-roam-dailies-goto-yesterday)
      ("dD" "Some date" org-roam-dailies-goto-date)]
     ;; remember org-roam-dailies-goto-{next,previous}-note

     ["State"
      ("s" "Choose a different org-roam project" fsta/org-roam-switch)
      ("S" "DB Sync" org-roam-db-sync)
      ("tb" "Toggle org-roam buffer" org-roam-buffer-toggle)
      ("G" "Generate and open a graph of the notes" org-roam-graph)
      ("od" "Open org-roam-directory" fsta/roam/open-directory)]]
    ["Org-mode"
     ;; TODO export this file as html
     [("C" "Capture" org-capture)
      ("g" "Create an id" org-id-get-create)
      ("ls" "Store link" org-store-link)]
     ["Download"
      ("y" "Paste (yank) clipboard using org-download" org-download-clipboard)]]
    ;; TODO create a "breeze transient" in breeze
    ["Breeze"
     :hide (lambda ()
             (not (and (fboundp 'breeze-listener-connected-p)
                       (breeze-listener-connected-p nil)
                       (fboundp 'breeze-capture))))
     [("bc" "Capture" fsta/note-taking-transient/breeze-capture)]]
    ;; This is used to run 1 command into another project without switching to it.
    ;; It's disbled because it's only supported by 3 commands (find, capture and insert)
    ;; ["Arguments" [(fsta/transient-option/org-roam-project)]]
    ["Essential commands"
     ("q" "Cancel" transient-quit-one)]))

;; (define-key fstamour-minor-mode-map (kbd "<f1>") 'fsta/note-taking-transient)
;; (fsta/call-with-org-roam-project 'org-roam-db-sync :project "public-notes")
;; (fsta/call-with-org-roam-project 'org-roam-node-find :project "public-notes")

(defun fsta/note-taking-transient ()
  "This is a stub to load the real transient...

This is a bit magic... calling this stub will for magit to
load (if it wasn't already loaded).  The the real command is
defined is a block that will be evaluated after magit is done
loading, that block also force org-roam to load.

Why magit? Because \"transient\" is \"part\" of magit."
  (interactive)
  (require 'magit)
  (call-interactively 'fsta/note-taking-transient))
