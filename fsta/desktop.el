(setf desktop-path '("~/.emacs.d/var/desktop"))

(custom-set-variables
 ;; Never confirm to save the desktop file, just save it
 '(desktop-save nil ;; t
                )
 ;; Don't load if the desktop file is locked
 '(desktop-load-locked-desktop nil))

;; automatically save the session when exiting emacs
(desktop-save-mode 1)

;; This hook is called when the desktop file is LOCKED _and_ is NOT
;; LOADED
;; 2023-02-06 - It looks like it's also called when the file doesn't exists...
(add-hook 'desktop-not-loaded-hook
          #'(lambda ()
              (warn "Disabled desktop-save-mode because the desktop file was locked.")
              (desktop-save-mode 0)))

;; Call this to manually save the desktop
;; (desktop-save desktop-dirname nil t)
;;
;; Use =M-x desktop-clear= to start fresh

(defun fsta/destkop-close-all ()
  "Empty the deskopt and save it."
  (interactive)
  (desktop-clear)
  (desktop-save desktop-dirname nil t))


;; (list (emacs-pid) (desktop-owner))
