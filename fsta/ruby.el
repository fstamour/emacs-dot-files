
;; ruby-mode is built-in (since emacs 23)

;; automatically insert "end"s
;; https://github.com/rejeep/ruby-end.el
(use-package ruby-end)

;; https://github.com/dgutov/robe
(use-package robe
  :config
  (add-hook 'ruby-mode-hook 'robe-mode))

;; https://github.com/nonsequitur/inf-ruby
;; I think inf-ruby (~slime) was installed as a dependecy of robe
(use-package inf-ruby)


;; Potentials:

;; emacs + tmux + ruby test
;; (use-package emacs-ruby-test)
;;
;; enh-ruby-mode
;; flymake-ruby
;; format-all
;; minitest
;; poly-ruby
;; rbtagger
;; realgud-{byebug,pry,rdb2}
;; rspec-mode
;; ruby-{

;; ruby-compilation               20150709.640   available    melpa    run a ruby process in a compilation buffer
;; ruby-electric                  20200328.1528  available    melpa    Minor mode for electrically editing ruby code
;; ruby-end                       0.4.3          available    gnu      Automatic insertion of end blocks for Ruby
;; ruby-end                       20230205.115   available    melpa    Automatic insertion of end blocks for Ruby
;; ruby-extra-highlight           20171106.1933  available    melpa    Highlight Ruby parameters.
;; ruby-factory                   20160102.721   available    melpa    Minor mode for Ruby test object generation libraries
;; ruby-hash-syntax               20210106.224   available    melpa    Toggle ruby hash syntax between => and 1.9+ styles
;; ruby-interpolation             20131112.1652  available    melpa    Ruby string interpolation helpers
;; ruby-json-to-hash              20211108.351   available    melpa    Convert JSON to Hash and play with the keys
;; ruby-refactor                  20160214.1650  available    melpa    A minor mode which presents various Ruby refactoring helpers.
;; ruby-test-mode                 20210205.1107  available    melpa    Minor mode for Behaviour and Test Driven
;; ruby-tools                     20151209.1615  available    melpa    Collection of handy functions for ruby-mode.

;; rufo (formatting)
;; seeing-is-believing
;; shoulda
;; simplecov
;; yard-mode

;; motion-mode for "ruby motion" ~= native apps in ruby

;; For rails:
;; https://github.com/asok/projectile-rails
;; https://github.com/eschulte/rinari
;; rail-{i18n,log-mode,routes
