;; THIS IS NOT LOADED
;; THIS IS LEGACY
;; TODO move to fsta/org-mode.el

(require 'org)

(add-hook 'org-mode-hook 'turn-on-auto-fill)

(require 'org-attach)
(require 'ox-publish)

(when (boundp 'use-package)
  (use-package htmlize)
  (require 'htmlize))



(setq org-startup-folded 'content)



;;; Capture
;;
;; - org-default-notes-file is defined in custom-set-variables in
;; init.el.
;; - in init.el, I set the global key `C-c c` to org-capture



;;; Links

;; resolve links to attachements
(setq org-link-abbrev-alist '(("att" . org-attach-expand-link)))
;; see to override files are opened by what: org-file-apps

(setq org-file-apps
      '(;; defaults:
        (auto-mode . emacs)
        ("\\.md\\'" . emacs)
        ("\\.mm\\'" . "freemind %s")
        ("\\.x?html?\\'" . default)
        ;; ("\\.pdf\\'" . default)
        ;; mine:
        ("\\.pdf\\'" . "mupdf %s")
        ("\\.mp4\\'" . "vlc %s")))

;; useful property: :ATTACH_DIR_INHERIT: t



;;; poporg - Emacs programming tool for editing strings or comments in
;;; Org mode or any other text mode

(use-package poporg)

;; Use the command "poporg-dwim"
;;
;; |--------------------------+------------+-----------------------------------------------------------------|
;; | Command                  | Keybinding | Description                                                     |
;; |--------------------------+------------+-----------------------------------------------------------------|
;; | *poporg-edit-exit*       | =C-x C-s=  | Save edit and kill poporg buffer                                |
;; | *poporg-update*          | =C-c C-c=  | Save edit and keep poporg buffer                                |
;; | *poporg-update-and-save* | =C-c C-s=  | Save edit and keep poporg buffer; then save the original buffer |
;; |--------------------------+------------+-----------------------------------------------------------------|
;; Add the time when a task is done.
(setq org-log-done t)
