(use-package run-command
  :bind ("<f5>" . run-command))

(defun fsta/run-command-recipe-executables ()
  "Run the current buffer's file if the file is executable.
Can also run it on every change using the external program \"entr\"."
  (let* ((buffer-file (buffer-file-name))
         (executable-p (and buffer-file (file-executable-p buffer-file))))
    (list
     (when executable-p
       (list
        :command-name "run-buffer-file"
        :command-line buffer-file
        :display "Run this buffer's file"))
     (when executable-p
       (list
        :command-name "run-buffer-file-watch"
        :command-line (format "echo %s | entr -r -c /_" buffer-file)
        :display "Run this buffer's file (re-run on each save)")))))

(defun fsta/find-makefile-dir ()
  "Search for a parent directory that contains a makefile."
  (or (locate-dominating-file default-directory "makefile")
      (locate-dominating-file default-directory "Makefile")))

(defun fsta/find-flake-dir ()
  "Search for a parent directory that contains a flake.nix file."
  (locate-dominating-file default-directory "flake.nix"))

(defun fsta/make-and-flake-recipes ()
  (let ((makep (fsta/find-makefile-dir))
        (flakep (fsta/find-flake-dir)))
    (list
     (and makep
          (list
           :display "Run make"
           :command-name "make"
           :command-line "make"
           :working-dir makep))
     (and flakep
          (list
           :display "Run nix build (flake)"
           :command-name "nix build"
           :command-line "nix build"
           :working-dir flakep))
     (and makep flakep
          ;; TODO Support the case where the makefile is in a subdirectory
          (string= makep flakep)
          (list
           :display "Run make in a nix develop environment."
           :command-name "make within flake"
           :command-line "nix develop -c make < /dev/null"
           :working-dir flakep)))))

(custom-set-variables
 '(run-command-recipes '(fsta/run-command-recipe-executables
                         fsta/make-and-flake-recipes)))

(fsta/comment
 (with-eval-after-load "run-command"
   (add-to-list 'run-command-recipes 'fsta/run-command-recipe-executables)
   (add-to-list 'run-command-recipes 'fsta/make-and-flake-recipes)))
