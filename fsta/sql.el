
(use-package sqlformat)

(setq sqlformat-command 'pgformatter
      sqlformat-args '("-s2" "-g"))
