
;;; Miscellaneous

;; Display a line at 80 characters, globally
(global-display-fill-column-indicator-mode 1)

;; Disable the welcome screen, emacs will show the =*scratch*= buffer
;; at startup instead.
(setq inhibit-startup-screen t)

;; Try to show the TODOs on startup
(fsta/comment
 (let ((path (expand-file-name "~/agenda.org")))
   (when (file-exists-p path)
     (setq initial-buffer-choice
           (lambda () (interactive)
             (org-agenda nil "t")
             (or (get-buffer "*Org Agenda*") t))))))



;;; Screen estate

;; Saving some screen estate by removing the line numbers.
;; (global-linum-mode -1)

;; I used to keep the menu on, because it's easier to discover stuff.
;; Now I hide it because I want less clutter
(menu-bar-mode -1)

;; We use the ~when~ condition because the functions don't exist when
;; starting emacs in a terminal.
;; disable the tool bar
(when tool-bar-mode
  (tool-bar-mode -1))
;; And hide the scrollbar
(when scroll-bar-mode
  (scroll-bar-mode -1))


;;; Highlighting
;;; - Highlight extraneous whitespaces.
;;; - Highlight mixes of spaces and tabs

(setq whitespace-style
      '(;; trailing
        space-before-tab
        ;; indentation
        space-after-tab
        tab-mark
        ;; space-mark
        ;; tabs
        )
      whitespace-line-column 80)

(global-whitespace-mode 1)

;; Highlight matching parenthesis
(show-paren-mode 1)


;;; Font

(defun fsta/has-font-p (font-name)
  (member
   (car (split-string font-name "-"))
   (font-family-list)))

;; TODO I don't have these fonts installed -_-
(fsta/comment
 (if-let ((font (car (cl-some
                      #'fsta/has-font-p
                      ;; Use the first font found
                      '("Fira Mono-14" "Roboto Mono")))))
     (set-frame-font
      font
      ;; Don't adjust the number of lines and columns to keep the same
      ;; frame size
      nil
      ;; Apply to all frames
      t)))

;; TODO https://www.reddit.com/r/emacs/comments/u5wcq5/change_dpi_when_moving_screens/i56c5jb/?utm_source=share&utm_medium=web3x&utm_name=web3xcss&utm_term=1&utm_content=share_button
;; 2023-09-19 was 150 (50% bigger than the default)
(defvar fsta/font-height 110)
(setf fsta/font-height 135)  ; I'm still undecided :P

(cl-defun fsta/set-face-attribute (&optional frame)
  (set-face-attribute 'default (or frame (selected-frame))
                      :height fsta/font-height))

;; (fsta/set-face-attribute)

;; Change the font size
(progn
  (fsta/set-face-attribute)

  (add-hook
   'after-make-frame-functions
   'fsta/set-face-attribute)

  (add-hook 'server-switch-hook 'fsta/set-face-attribute))

;; On Windows, you can use =(w32-select-font)= to help get the right
;; font name.


;;; Cursor

(set-default 'cursor-type '(bar . 2))


;;; Scrolling

(pixel-scroll-mode 1)

;; For when emacs 29 is released:
;; pixel-scroll-precision-mode
;; pixel-scroll-precision-use-momentum
;; pixel-scroll-precision-interpolate-page
