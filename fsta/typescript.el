
;; use M-x typescript-ts-mode
;; and M-x eglot

(add-to-list 'auto-mode-alist '("\\.ts\\'" . typescript-ts-mode))

(with-eval-after-load 'eglot
  (add-hook 'typescript-ts-mode-hook 'eglot-ensure)
  (add-hook 'typescript-ts-mode-hook 'prettier-mode))


(fsta/comment
 eglot-server-programs


 (add-to-list
  'eglot-server-programs
  `(((js-mode :language-id "javascript")
     (js-ts-mode :language-id "javascript")
     (tsx-ts-mode :language-id "typescriptreact")
     (typescript-ts-mode :language-id "typescript")
     (typescript-mode :language-id "typescript"))
    .
    ("tsserver" "--stdio")))

 ;; Example on how to send different "initialization options"
 (add-to-list
  'eglot-server-programs
  `(((js-mode :language-id "javascript")
     (js-ts-mode :language-id "javascript")
     (tsx-ts-mode :language-id "typescriptreact")
     (typescript-ts-mode :language-id "typescript")
     (typescript-mode :language-id "typescript"))
    .
    ("typescript-language-server" "--stdio"
     :initializationOptions
     ;; This is wrong, those aren't options, they're code actions
     (:source (:organizeImports t)))))


 (cl-first eglot-server-programs)
 (pop eglot-server-programs)

 (eglot-code-action-organize-imports))
