;; show formfeed characters as lines
(use-package page-break-lines
  :config (global-page-break-lines-mode 1))
