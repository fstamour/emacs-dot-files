
;; Manage Files with Emacs: Organise your drive with Dired - https://lucidmanager.org/productivity/manage-files-with-emacs/

;; Try to guess a default target directory (e.g. when copyin or
;; renaming)
(setf dired-dwim-target
      'dired-dwim-target-recent)

(fsta/comment
 ;; This is the default
 (define-key dired-mode-map (kbd "R") 'dired-do-rename)

 (cl-defun fsta/dired-do-rename (&optional arg)
   (interactive "P")
   (message "fsta/dired-do-rename's args: %S" arg)
   (dired-do-rename arg))

 (define-key dired-mode-map (kbd "R") 'fsta/dired-do-rename))
