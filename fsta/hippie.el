;;;; Configuring hippie-expand
;;;; https://www.emacswiki.org/emacs/HippieExpand

;;
;; TODO See ispell-dictionary-alist
;;

(fsta/comment
 (setf fsta/hippie-expand-try-functions-list/original
       hippie-expand-try-functions-list))

;; Was already t...
;; (setq hippie-expand-verbose t)


;; ispell-lookup-words uses a plain-text dictionary here we use the
;; one from https://savannah.gnu.org/projects/miscfiles/ (install with
;; guix-home) I have some others in (expand-file-name
;; "~/dotfiles/data/..."), but they don't have the right format.
(setf ispell-alternate-dictionary
      (expand-file-name "~/.guix-home/profile/share/web2"))
;; TODO see ispell-complete-word-dict

(defun fsta/try-complete-spell (old)
  "A hippie-expand-try-function that suggest expansions from a wordlist (using ispell-lo "
  ;; "old" is nil the first time
  (unless old
    ;; tell hippie-expand which region to replace by the expansion
    ;; also initialise the "he-search-string" variable.
    (he-init-string (he-lisp-symbol-beg) (point))
    ;; add the the current region to the list of already-tried
    ;; expansions
    (unless (he-string-member he-search-string he-tried-table)
      (setq he-tried-table (cons he-search-string he-tried-table)))
    ;; find the list of suggestions...
    (setq he-expand-list
          (and (not (equal he-search-string ""))
               ;; ~~try ispell-complete-word or
               ;; ispell-complete-word-interior-frag~~ nevermind, they
               ;; use ispell-lookup-words internally...
               (ispell-lookup-words
                (concat he-search-string "*")
                ispell-alternate-dictionary))))
  ;; (message "he-expand-list: %s" he-expand-list)
  (if he-expand-list
      (progn
        ;; do the substitution
        (he-substitute-string (car he-expand-list))
        ;; remove the expansion from the list
        (setq he-expand-list (cdr he-expand-list))
        ;; tell hippie-expand we're good so far
        t)
    (when old (he-reset-string))))

;; Add 'fsta/try-complete-spell to the end, if not already in the list
(unless (member 'fsta/try-complete-spell
                hippie-expand-try-functions-list)
  (setf hippie-expand-try-functions-list
        (append hippie-expand-try-functions-list
                '(fsta/try-complete-spell))))


;; TODO Look into make-hippie-expand-function, for one-shot, "dynamic"
;; hippie-expand command.
