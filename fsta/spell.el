
(setq ispell-program-name "hunspell")

(fsta/comment
 (getenv "LANG")

 (getenv "DICTIONARY")

 ;; DICPATH is missing the "guix-home" profile
 (getenv "DICPATH")

 (setenv "DICTIONARY" "en_GB"))
