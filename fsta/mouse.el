
(define-key
  help-mode-map
  [mouse-8]
  'help-go-back)

(define-key
  help-mode-map
  [mouse-9]
  'help-go-forward)

(define-key
  help-mode-map
  [mouse-4]
  'scroll-down)

(define-key
  help-mode-map
  [mouse-5]
  'scroll-up)

;; TODO Add similar bindings to slime/sly modes (e.g. in the inspector)
