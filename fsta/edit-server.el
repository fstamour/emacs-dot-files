;; To use with 'edit-with-emacs' extension
;; https://addons.mozilla.org/en-US/firefox/addon/edit-with-emacs1/
;; https://www.emacswiki.org/emacs/Edit_with_Emacs
;; N.B. It is safe to call (edit-server-start) multiple times

(use-package edit-server
  :ensure t
  :commands edit-server-start
  :init (if after-init-time
            (edit-server-start)
          (add-hook 'after-init-hook
                    #'(lambda() (edit-server-start))))
  :config (setq edit-server-new-frame-alist
                '((name . "Edit with Emacs FRAME")
                  (minibuffer . t)
                  (menu-bar-lines . t)
                  (window-system . x))
                edit-server-url-major-mode-alist
                '(("github\\.com" . markdown-mode)
                  ("gitlab.*\\.com" . markdown-mode))))
