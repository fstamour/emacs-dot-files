(require 'cl-lib)

;; Stop flooding me with warnings!
(setq native-comp-async-report-warnings-errors nil)

(defvar fsta/load-times ())

;; Sort longest time first (magit won, by far)
;; (setf fsta/load-times (cl-sort fsta/load-times #'> :key #'cdr))

;; (cl-loop for (filename . time) in fsta/load-times sum time)
;; => roughly 4 seconds

(cl-defmacro fsta/measure-time* (body
                                 ((elapsed-time-var &optional (result-var (gensym "result-var")))
                                  &body handler-body))
  "Measure the time it takes to evaluate BODY."
  `(let* ((time (current-time)) ;; TODO Time should be gensym'd
          (,result-var ,body)
          (,elapsed-time-var (float-time (time-since time))))
     ,@handler-body))

(defvar fsta/loaded-files '()
  "Variable to keep track of all the files loaded by fsta/load.")

;; TODO not tested
(defun fsta/eval-after-load (features thunk)
  "Eval THUNK after every FEATURES are on."
  (let ((fn (lambda ()
              (when (cl-every 'featurep features)
                thunk))))
    (mapc (lambda (feature)
            (eval-after-load feature fn)))))

;; TODO rename to "disabled or autoloaded"
(defvar fsta/disabled-files '(;; smerge is auto-loaded
                              "fsta/smerge.el")
  "Variable to keep track of all the files explicitly NOT loaded by fsta/load.")

;; TODO currently `enabled' is either `nil' or `t', but it could be a
;; "feature" which could be turned into a call to `eval-after-load'. Furthermore, if it's a list, it could
(defun fsta/load (enabled &rest filenames)
  (unless enabled
    (setf fsta/disabled-files (append fsta/disabled-files filenames)))
  (when enabled
    (mapc
     #'(lambda (filename)
         (message "Loading %S..." filename)
         (fsta/measure-time*
          (load
           (expand-file-name
            filename user-emacs-directory))
          ((elapsed-time)
           (let ((result (cons filename elapsed-time)))
             (push result fsta/load-times)
             result)))
         (message "Loaded %S." filename))
     filenames)
    (setf fsta/loaded-files (append fsta/loaded-files filenames))))
;; TODO Split into:
;; 1. load
;; 2. load with timing
;; 3. load but takes a symbol that represents the file name (without the "fsta/" prefix)
;; 4. load, specifying the dependencies (à la asdf)... maybe?

(defvar *enable-offline-clhs* nil
  "Used by fsta/lisp IIRC.")

;;; Customizations
(custom-set-variables
 '(org-default-notes-file "~/notes/captured.org")
 '(org-directory "~/notes/"))

;; Add "magit" to project's "switch-project" command before magit is
;; even loaded (this will autoload
;; magit). P.S. `project-switch-commands' is used by
;; `project-switch-project'
(with-eval-after-load 'project
  (define-key project-prefix-map "m" 'magit-project-status)
  (add-to-list 'project-switch-commands '(magit-project-status "Magit") t))

;; TODO don't load everything depending on command-line-args
;; e.g. ("/path/to/emacs" "-f" "message-mailto" "mailto:example@example.com")

;; load basic stuff (i.e. stuff that doesn't need extra packages)
(fsta/load t
           "fsta/utils.el"
           "fsta/base.el"
           "fsta/compile.el"
           "fsta/minibuffer.el"
           "fsta/desktop.el"
           "fsta/clipboard.el"
           "fsta/insert-current-date.el"
           "fsta/eldoc.el"
           "fsta/aesthetics.el"
           "fsta/abbrev.el"
           "fsta/dired.el"
           "fsta/history.el"
           "fsta/spell.el"
           "fsta/hippie.el"
           "fsta/winner.el")

;; Load the rest™
(fsta/load nil
           "fsta/aggressive-indent.el"
           "fsta/tempel.el")

(fsta/load t
           "fsta/ace-window.el"
           "fsta/vertico.el"
           "fsta/prescient.el"
           "fsta/tree-sitter.el")

(with-eval-after-load 'smerge-mode
  (fsta/load t "fsta/smerge.el"))

(fsta/load nil "fsta/magit.el")              ; this one is autoloaded

;; org-mode stuff
(fsta/load t
           "fsta/org-mode.el"
           "fsta/org-babel.el"
           "fsta/org-download.el"
           "fsta/org-roam.el")

(fsta/load nil
           "fsta/obsidian.el")

(fsta/load nil
           "fsta/projectile.el"
           "fsta/mouse.el"
           "fsta/page-break-lines.el")

(fsta/load t "fsta/lisp.el")

(fsta/load t "fsta/go.el")

(fsta/load t
           "fsta/prettier.el"
           "fsta/typescript.el")

(fsta/load nil
           "fsta/cmake.el"
           "fsta/fish.el"
           "fsta/forth.el"
           "fsta/html.el"
           "fsta/lsp.el"
           "fsta/markdown.el"
           "fsta/nix.el"

           "fsta/python.el"
           "fsta/sql.el"
           "fsta/terraform.el"
           "fsta/yaml.el")

(fsta/load nil
           "fsta/deadgrep.el"
           "fsta/docker.el"

           "fsta/server.el"
           "fsta/edit-server.el"

           "fsta/quicksearch.el"
           "fsta/run-command.el")

;; All the files that are there are I never loaded :D
(fsta/load nil
           "fsta/auto-insert.el"
           "fsta/erc.el"
           "fsta/gitlab.el"
           "fsta/helm.el"
           "fsta/json.el"
           "fsta/jupyter.el"
           "fsta/mpd.el"
           "fsta/org-mode-configuration.el"
           "fsta/org-super-agenda.el"
           "fsta/ruby.el"
           "fsta/shell.el"
           "fsta/sly.el"
           "fsta/smooth-scrolling.el")

(fsta/load t
           "fsta/vscode.el"
           "fsta/fsta-mode.el")

(fsta/load t
           ;; Keep these at the end (see https://github.com/purcell/envrc)
           "fsta/direnv.el"
           "fsta/diminish.el")



;; I often create new files in "fsta/" folder, but forget to add them
;; to the init.el
(let* ((files
        ;; List the files under the "fsta/" directory
        (mapcar #'(lambda (file) (concat "fsta/" file))
                (directory-files
                 (concat (file-name-directory user-init-file) "fsta")
                 nil                    ; full
                 (rx string-start (+ (any alpha "-")) ".el" string-end))))
       ;; all-files minus loaded-files
       (maybe-forgotten-files
        (cl-set-difference files fsta/loaded-files :test #'string=))
       (forgotten-files (cl-set-difference
                         maybe-forgotten-files
                         fsta/disabled-files
                         ;; '("fsta/helm.el")
                         :test #'string=)))
  (when forgotten-files
    (warn "There are some files in \"fsta/\" that are not loaded: %s"
          (mapconcat #'(lambda (file)
                         (concat "\n  - " file))
                     forgotten-files
                     ""))))

(message "Done loading init.el")
